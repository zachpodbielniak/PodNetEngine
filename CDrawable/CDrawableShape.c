#include "CDrawableShape.h"


typedef struct __DRAWABLE_SHAPE
{
	INHERITS_FROM_DRAWABLE();
	LPSHAPE 	lpshpShape;
	BOOL 		bFill;
} DRAWABLE_SHAPE, *LPDRAWABLE_SHAPE;




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DrawShape(
	_In_ 		HDRAWABLE		hdDrawable,
	_In_Opt_ 	LPVOID 			lpParam
){
	UNREFERENCED_PARAMETER(lpParam);
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE_SHAPE lpShape;
	LPVOID lpChild;
	COLOR clrWhite;

	lpShape = (LPDRAWABLE_SHAPE)hdDrawable;
	lpChild = lpShape->lpshpShape->lpChild;
	

	clrWhite.byRed = 255;
	clrWhite.byGreen = 255;
	clrWhite.byBlue = 255;
	clrWhite.byAlpha = 255;

	switch ((ULONG)lpShape->lpshpShape->ulShapeType)
	{
		case (ULONG)SHAPE_CIRCLE:
		{
			DrawCircle(
				lpShape->lpDrawable->hParentRenderer,
				lpShape->lpshpShape->corCoordinate.dX,
				lpShape->lpshpShape->corCoordinate.dY,
				((LPCIRCLE)lpChild)->dbRadius,
				clrWhite,
				lpShape->bFill
			);

			break;
		}

		case (ULONG)SHAPE_RECTANGLE:
		{
			DrawRectangle(
				lpShape->lpDrawable->hParentRenderer,
				lpShape->lpshpShape->corCoordinate.dX,
				lpShape->lpshpShape->corCoordinate.dY,
				((LPRECTANGLE)lpChild)->dbBase,
				((LPRECTANGLE)lpChild)->dbHeight,
				(FLOAT)lpShape->lpshpShape->dbRotation,
				clrWhite,
				lpShape->bFill
			);

			break;
		}

		default: return FALSE;
	}

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyShape(
	_In_ 		HDRAWABLE		hdDrawable
){
	LPDRAWABLE_SHAPE lpShape;
	lpShape = (LPDRAWABLE_SHAPE)hdDrawable;

	DestroyShape(lpShape->lpshpShape);
	FreeMemory(lpShape);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__SetShapeData(
	_In_ 		HDRAWABLE		hdDrawable,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_ 		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight,
	_In_ 		BOOL 			bOffset
){
	UNREFERENCED_PARAMETER(ulHeight);

	LPDRAWABLE_SHAPE lpShape;
	LPVOID lpChild;
	lpShape = (LPDRAWABLE_SHAPE)hdDrawable;
	lpChild = lpShape->lpshpShape->lpChild;

	switch (lpShape->lpshpShape->ulShapeType)
	{

		case (ULONG)SHAPE_CIRCLE:
		{
			
			if (TRUE == bOffset)
			{
				if (MAX_ULONG != ulWidth)
				{ ((LPCIRCLE)lpChild)->dbRadius += (DOUBLE)(LONG)ulWidth; }

				if (MAX_ULONG != ulPositionX)
				{ lpShape->lpshpShape->corCoordinate.dX += (DOUBLE)(LONG)ulPositionX; }

				if (MAX_ULONG != ulPositionY)
				{ lpShape->lpshpShape->corCoordinate.dY += (DOUBLE)(LONG)ulPositionY; }
			}
			else
			{
				if (MAX_ULONG != ulWidth)
				{ ((LPCIRCLE)lpChild)->dbRadius = (DOUBLE)(LONG)ulWidth; }

				if (MAX_ULONG != ulPositionX)
				{ lpShape->lpshpShape->corCoordinate.dX = (DOUBLE)(LONG)ulPositionX; }

				if (MAX_ULONG != ulPositionY)
				{ lpShape->lpshpShape->corCoordinate.dY = (DOUBLE)(LONG)ulPositionY; }
			}

			break;
		}

		case (ULONG)SHAPE_RECTANGLE:
		{
			if (TRUE == bOffset)
			{
				if (MAX_ULONG != ulWidth)
				{ ((LPRECTANGLE)lpChild)->dbBase += (DOUBLE)(LONG)ulWidth; }
				
				if (MAX_ULONG != ulHeight)
				{ ((LPRECTANGLE)lpChild)->dbHeight += (DOUBLE)(LONG)ulHeight; }

				if (MAX_ULONG != ulPositionX)
				{ lpShape->lpshpShape->corCoordinate.dX += (DOUBLE)(LONG)ulPositionX; }
				
				if (MAX_ULONG != ulPositionY)
				{ lpShape->lpshpShape->corCoordinate.dY += (DOUBLE)(LONG)ulPositionY; }
			}
			else
			{
				if (MAX_ULONG != ulWidth)
				{ ((LPRECTANGLE)lpChild)->dbBase = (DOUBLE)(LONG)ulWidth; }
				
				if (MAX_ULONG != ulHeight)
				{ ((LPRECTANGLE)lpChild)->dbHeight = (DOUBLE)(LONG)ulHeight; }

				if (MAX_ULONG != ulPositionX)
				{ lpShape->lpshpShape->corCoordinate.dX = (DOUBLE)(LONG)ulPositionX; }
				
				if (MAX_ULONG != ulPositionY)
				{ lpShape->lpshpShape->corCoordinate.dY = (DOUBLE)(LONG)ulPositionY; }
			}

			break;
		}

	}
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__GetShapeData(
	_In_ 		HDRAWABLE		hdDrawable,
	_Out_ 		LPULONG 		lpulPositionX,
	_Out_ 		LPULONG			lpulPositionY,
	_Out_ 		LPULONG			lpulWidth,
	_Out_ 		LPULONG 		lpulHeight
){
	LPDRAWABLE_SHAPE lpShape;
	LPVOID lpChild;
	LONG lValue;
	lpShape = (LPDRAWABLE_SHAPE)hdDrawable;

	lpChild = lpShape->lpshpShape->lpChild;

	switch (lpShape->lpshpShape->ulShapeType)
	{
		
		case SHAPE_CIRCLE:
		{
			if (NULLPTR != lpulPositionX)
			{ 
				lValue = (LONG)lpShape->lpshpShape->corCoordinate.dX;
				*lpulPositionX = lValue;
			}

			if (NULLPTR != lpulPositionY)
			{ 
				lValue = (LONG)lpShape->lpshpShape->corCoordinate.dY;
				*lpulPositionY = lValue;
			}

			if (NULLPTR != lpulWidth)
			{ *lpulWidth = (LONG)((LPCIRCLE)(lpChild))->dbRadius; }

			if (NULLPTR != lpulHeight)
			{ *lpulHeight = 0; }

			break;
		}

		case SHAPE_RECTANGLE:
		{
			if (NULLPTR != lpulPositionX)
			{ 
				lValue = (LONG)lpShape->lpshpShape->corCoordinate.dX;
				*lpulPositionX = lValue;
			}

			if (NULLPTR != lpulPositionY)
			{ 
				lValue = (LONG)lpShape->lpshpShape->corCoordinate.dY;
				*lpulPositionY = lValue;
			}

			if (NULLPTR != lpulWidth)
			{ *lpulWidth = (LONG)((LPRECTANGLE)(lpChild))->dbBase; }

			if (NULLPTR != lpulHeight)
			{ *lpulWidth = (LONG)((LPRECTANGLE)(lpChild))->dbHeight; }
			
			break;
		}

	}

	return TRUE;
}
_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__SetShapeRotation(
	_In_ 		HDRAWABLE 		hdDrawable,
	_In_ 		FLOAT 			fTheta,
	_In_ 		BOOL 			bOffset
){
	LPDRAWABLE_SHAPE lpShape;
	lpShape = (LPDRAWABLE_SHAPE)hdDrawable;
	
	if (FALSE == bOffset)
	{ lpShape->lpshpShape->dbRotation = (DOUBLE)fTheta; }
	else
	{ lpShape->lpshpShape->dbRotation += (DOUBLE)fTheta; }

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__GetShapeRotation(
	_In_ 		HDRAWABLE 		hdDrawable,
	_Out_ 		LPFLOAT 		lpTheta
){
	LPDRAWABLE_SHAPE lpShape;
	lpShape = (LPDRAWABLE_SHAPE)hdDrawable;

	*lpTheta = (FLOAT)lpShape->lpshpShape->dbRotation;
	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
HDRAWABLE
CreateDrawableShape(
	_In_ 		HANDLE 			hRenderer,
	_In_ 		SHAPE_TYPE 		stType,
	_In_ 		BOOL 			bFill
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, NULLPTR);
	EXIT_IF_UNLIKELY_VALUE(SHAPE_UNDEFINED, stType, NULLPTR);
	EXIT_IF_UNLIKELY_VALUE(SHAPE_NOT_A_SHAPE, stType, NULLPTR);

	LPDRAWABLE_SHAPE lpShape;
	lpShape = GlobalAllocAndZero(sizeof(DRAWABLE_SHAPE));
	EXIT_IF_UNLIKELY_NULL(lpShape, NULLPTR);

	switch ((ULONG)stType)
	{
	
		case (ULONG)SHAPE_CIRCLE:
		{
			lpShape->lpshpShape = CreateShape(0.0, 0.0, 0.0, SHAPE_CIRCLE);
			break;
		}

		case (ULONG)SHAPE_RECTANGLE:
		{
			lpShape->lpshpShape = CreateShape(0.0, 0.0, 0.0, SHAPE_RECTANGLE);
			break;
		}

		case (ULONG)SHAPE_TRIANGLE:
		{
			lpShape->lpshpShape = CreateShape(0.0, 0.0, 0.0, SHAPE_TRIANGLE);			
			break;
		}

	}

	lpShape->bFill = bFill;
	lpShape->dtType = DRAWABLE_TYPE_SHAPE;
	lpShape->lpDrawable = GlobalAllocAndZero(sizeof(DRAWABLE));
	EXIT_IF_UNLIKELY_NULL(lpShape->lpDrawable, NULLPTR);

	InitializeDrawable(
		hRenderer,
		lpShape->lpDrawable,
		DRAWABLE_TYPE_SHAPE,
		lpShape,
		__DrawShape,
		__DestroyShape,
		__GetShapeData,
		__SetShapeData,
		__GetShapeRotation,
		__SetShapeRotation
	);

	return lpShape;
}