#include "CDrawableImage.h"


typedef struct __DRAWABLE_IMAGE
{
	INHERITS_FROM_DRAWABLE();
	LPTEXTURE		lptxTexture;
	FLOAT 			fTheta;
} DRAWABLE_IMAGE, *LPDRAWABLE_IMAGE;



_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DrawImage(
	_In_ 		HDRAWABLE		hdDrawable,
	_In_Opt_ 	LPVOID 			lpParam
){
	UNREFERENCED_PARAMETER(lpParam);
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE_IMAGE lpImage;
	lpImage = (LPDRAWABLE_IMAGE)hdDrawable;
	if (FALSE == FLOAT_EQUAL(0.0f, lpImage->fTheta))
	{ 
		return AddTextureToRenderBufferAndRotate(
			lpImage->lpDrawable->hParentRenderer, 
			lpImage->lptxTexture, 
			lpImage->fTheta
		); 
	}

	return AddTextureToRenderBuffer(lpImage->lpDrawable->hParentRenderer, lpImage->lptxTexture);	
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyImage(
	_In_ 		HDRAWABLE		hdDrawable
){
	LPDRAWABLE_IMAGE lpImage;
	lpImage = (LPDRAWABLE_IMAGE)hdDrawable;

	DestroyTexture(lpImage->lptxTexture);
	FreeMemory(lpImage);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__SetImageData(
	_In_ 		HDRAWABLE		hdDrawable,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_ 		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight,
	_In_ 		BOOL 			bOffset
){
	LPDRAWABLE_IMAGE lpImage;
	lpImage = (LPDRAWABLE_IMAGE)hdDrawable;

	switch (bOffset)
	{

		case TRUE:
		{
			if (MAX_ULONG != ulHeight)
			{ lpImage->lptxTexture->wrectCoordinates.ulH += (LONG)ulHeight; }

			if (MAX_ULONG != ulWidth)
			{ lpImage->lptxTexture->wrectCoordinates.ulW += (LONG)ulWidth; }

			if (MAX_ULONG != ulPositionX)
			{ lpImage->lptxTexture->wrectCoordinates.ulX += (LONG)ulPositionX; }

			if (MAX_ULONG != ulPositionY)
			{ lpImage->lptxTexture->wrectCoordinates.ulY += (LONG)ulPositionY; }
			break;
		}

		case FALSE:
		{
			if (MAX_ULONG != ulHeight)
			{ lpImage->lptxTexture->wrectCoordinates.ulH = ulHeight; }

			if (MAX_ULONG != ulWidth)
			{ lpImage->lptxTexture->wrectCoordinates.ulW = ulWidth; }

			if (MAX_ULONG != ulPositionX)
			{ lpImage->lptxTexture->wrectCoordinates.ulX = ulPositionX; }

			if (MAX_ULONG != ulPositionY)
			{ lpImage->lptxTexture->wrectCoordinates.ulY = ulPositionY; }
		}

	}


	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__GetImageData(
	_In_ 		HDRAWABLE		hdDrawable,
	_Out_ 		LPULONG 		lpulPositionX,
	_Out_ 		LPULONG			lpulPositionY,
	_Out_ 		LPULONG			lpulWidth,
	_Out_ 		LPULONG 		lpulHeight
){
	LPDRAWABLE_IMAGE lpImage;
	lpImage = (LPDRAWABLE_IMAGE)hdDrawable;

	if (NULLPTR != lpulPositionX)
	{ *lpulPositionX = lpImage->lptxTexture->wrectCoordinates.ulX; }

	if (NULLPTR != lpulPositionY)
	{ *lpulPositionY = lpImage->lptxTexture->wrectCoordinates.ulY; }
	
	if (NULLPTR != lpulWidth)
	{ *lpulWidth = lpImage->lptxTexture->wrectCoordinates.ulW; }

	if (NULLPTR != lpulHeight)
	{ *lpulHeight = lpImage->lptxTexture->wrectCoordinates.ulH; }

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__SetImageRotation(
	_In_ 		HDRAWABLE 		hdDrawable,
	_In_ 		FLOAT 			fTheta,
	_In_ 		BOOL 			bOffset
){
	LPDRAWABLE_IMAGE lpImage;
	lpImage = (LPDRAWABLE_IMAGE)hdDrawable;

	if (FALSE == bOffset)
	{ lpImage->fTheta = fTheta; }
	else
	{ lpImage->fTheta += fTheta; }

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__GetImageRotation(
	_In_ 		HDRAWABLE 		hdDrawable,
	_Out_ 		LPFLOAT			lpfTheta
){
	LPDRAWABLE_IMAGE lpImage;
	lpImage = (LPDRAWABLE_IMAGE)hdDrawable;

	*lpfTheta = lpImage->fTheta;
	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
HDRAWABLE
CreateDrawableImage(
	_In_ 		HANDLE 			hRenderer,
	_In_ 		LPSTR RESTRICT 		lpszFile
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpszFile, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, NULLPTR);

	LPDRAWABLE_IMAGE lpImage;
	lpImage = GlobalAllocAndZero(sizeof(DRAWABLE_IMAGE));
	EXIT_IF_UNLIKELY_NULL(lpImage, NULLPTR);

	lpImage->dtType = DRAWABLE_TYPE_IMAGE;
	lpImage->lpDrawable = GlobalAllocAndZero(sizeof(DRAWABLE));
	EXIT_IF_LIKELY_NULL(lpImage->lpDrawable, NULLPTR);

	InitializeDrawable(
		hRenderer,
		lpImage->lpDrawable,
		DRAWABLE_TYPE_IMAGE,
		lpImage,
		__DrawImage,
		__DestroyImage,
		__GetImageData,
		__SetImageData,
		__GetImageRotation,
		__SetImageRotation
	);

	lpImage->lptxTexture = CreateTexture(
		hRenderer,
		lpszFile,
		TEXTURE_FILE_PNG	/* TODO: Fix This */
	);

	QueryTexture(lpImage->lptxTexture);

	return lpImage;
}
