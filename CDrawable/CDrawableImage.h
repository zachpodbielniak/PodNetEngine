#ifndef PODNETENGINE_DRAWABLETEXTURE_H
#define PODNETENGINE_DRAWABLETEXTURE_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"
#include "CDrawable.h"
#include "../CTexture/CTexture.h"
#include "../CRenderer/CRenderer.h"



typedef struct __DRAWABLE_TEXTURE	DRAWABLE_TEXTURE, *LPDRAWABLE_TEXTURE, *HTEXTURE;






_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
HDRAWABLE
CreateDrawableImage(
	_In_ 		HANDLE 			hRenderer,
	_In_ 		LPSTR RESTRICT 		lpszFile
);








#endif