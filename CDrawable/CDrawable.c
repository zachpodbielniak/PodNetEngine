#include "CDrawable.h"




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
InitializeDrawable(
	_In_ 		HANDLE 				hParentRenderer,
	_In_ 		LPDRAWABLE			lpThis,
	_In_ 		DRAWABLE_TYPE 			dtDrawType,
	_In_ 		LPVOID 				lpParent,
	_In_ 		LPFN_DRAWABLE_DRAW_PROC		lpfnDraw,
	_In_ 		LPFN_DRAWABLE_DESTROY_PROC	lpfnDestroy,
	_In_ 		LPFN_DRAWABLE_GET_PROC		lpfnGet,
	_In_ 		LPFN_DRAWABLE_SET_PROC		lpfnSet,
	_In_Opt_	LPFN_DRAWABLE_GET_THETA_PROC	lpfnGetTheta,
	_In_Opt_	LPFN_DRAWABLE_SET_THETA_PROC	lpfnSetTheta
){
	EXIT_IF_UNLIKELY_NULL(hParentRenderer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hParentRenderer), HANDLE_TYPE_RENDERER, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpThis, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpParent, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnDraw, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnDestroy, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnGet, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpfnSet, FALSE);

	lpThis->hParentRenderer = hParentRenderer;
	lpThis->dtDrawType = dtDrawType;
	lpThis->lpParent = lpParent;
	lpThis->lpfnDraw = lpfnDraw;
	lpThis->lpfnDestroy = lpfnDestroy;
	lpThis->lpfnGet = lpfnGet;
	lpThis->lpfnSet = lpfnSet;
	lpThis->lpfnGetTheta = lpfnGetTheta;
	lpThis->lpfnSetTheta = lpfnSetTheta;

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
Draw(
	_In_ 		HDRAWABLE			hdDrawable,
	_In_Opt_	LPVOID 				lpParam
){
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE lpDrawable;
	lpDrawable = ((DLPDRAWABLE)hdDrawable)[0];
	EXIT_IF_UNLIKELY_NULL(lpDrawable, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDrawable->lpfnDraw, FALSE);

	return lpDrawable->lpfnDraw(hdDrawable, lpParam);
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
DestroyDrawable(
	_In_ 		HDRAWABLE			hdDrawable
){
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE lpDrawable;
	lpDrawable = ((DLPDRAWABLE)hdDrawable)[0];
	EXIT_IF_UNLIKELY_NULL(lpDrawable, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDrawable->lpfnDestroy, FALSE);

	return lpDrawable->lpfnDestroy(hdDrawable);
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
SetDrawablePosition(
	_In_		HDRAWABLE			hdDrawable,
	_In_ 		ULONG 				ulPositionX,
	_In_ 		ULONG 				ulPositionY
){
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE lpDrawable;
	lpDrawable = ((DLPDRAWABLE)hdDrawable)[0];
	EXIT_IF_UNLIKELY_NULL(lpDrawable, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDrawable->lpfnSet, FALSE);

	return lpDrawable->lpfnSet(
		hdDrawable,
		ulPositionX,
		ulPositionY,
		MAX_ULONG,
		MAX_ULONG,
		FALSE
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
SetDrawableSize(
	_In_ 		HDRAWABLE 			hdDrawable,
	_In_ 		ULONG 				ulWidth,
	_In_ 		ULONG 				ulHeight
){
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE lpDrawable;
	lpDrawable = ((DLPDRAWABLE)hdDrawable)[0];
	EXIT_IF_UNLIKELY_NULL(lpDrawable, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDrawable->lpfnSet, FALSE);

	return lpDrawable->lpfnSet(
		hdDrawable,
		MAX_ULONG,
		MAX_ULONG,
		ulWidth,
		ulHeight, 
		FALSE
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
SetDrawablePositionByOffset(
	_In_		HDRAWABLE			hdDrawable,
	_In_ 		ULONG 				ulPositionX,
	_In_ 		ULONG 				ulPositionY
){
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE lpDrawable;
	lpDrawable = ((DLPDRAWABLE)hdDrawable)[0];
	EXIT_IF_UNLIKELY_NULL(lpDrawable, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDrawable->lpfnSet, FALSE);

	return lpDrawable->lpfnSet(
		hdDrawable,
		ulPositionX,
		ulPositionY,
		MAX_ULONG,
		MAX_ULONG,
		TRUE
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
GetDrawablePosition(
	_In_		HDRAWABLE			hdDrawable,
	_Out_ 		LPULONG				lpulPositionX,
	_Out_ 		LPULONG				lpulPositionY
){
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE lpDrawable;
	lpDrawable = ((DLPDRAWABLE)hdDrawable)[0];
	EXIT_IF_UNLIKELY_NULL(lpDrawable, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDrawable->lpfnGet, FALSE);

	return lpDrawable->lpfnGet(
		hdDrawable,
		lpulPositionX,
		lpulPositionY,
		NULLPTR,
		NULLPTR
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
GetDrawableSize(
	_In_		HDRAWABLE			hdDrawable,
	_Out_ 		LPULONG				lpulWidth,
	_Out_ 		LPULONG				lpulHeight
){
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE lpDrawable;
	lpDrawable = ((DLPDRAWABLE)hdDrawable)[0];
	EXIT_IF_UNLIKELY_NULL(lpDrawable, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDrawable->lpfnGet, FALSE);

	return lpDrawable->lpfnGet(
		hdDrawable,
		NULLPTR,
		NULLPTR,
		lpulWidth,
		lpulHeight
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
SetDrawablePositionAndSize(
	_In_		HDRAWABLE			hdDrawable,
	_In_ 		ULONG 				ulPositionX,
	_In_ 		ULONG 				ulPositionY,
	_In_ 		ULONG 				ulWidth,
	_In_ 		ULONG 				ulHeight
){
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE lpDrawable;
	lpDrawable = ((DLPDRAWABLE)hdDrawable)[0];
	EXIT_IF_UNLIKELY_NULL(lpDrawable, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDrawable->lpfnSet, FALSE);

	return lpDrawable->lpfnSet(
		hdDrawable,
		ulPositionX,
		ulPositionY,
		ulWidth,
		ulHeight,
		FALSE
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
GetDrawablePositionAndSize(
	_In_		HDRAWABLE			hdDrawable,
	_Out_ 		LPULONG				lpulPositionX,
	_Out_ 		LPULONG				lpulPositionY,
	_Out_ 		LPULONG				lpulWidth,
	_Out_ 		LPULONG				lpulHeight
){
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE lpDrawable;
	lpDrawable = ((DLPDRAWABLE)hdDrawable)[0];
	EXIT_IF_UNLIKELY_NULL(lpDrawable, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDrawable->lpfnGet, FALSE);

	return lpDrawable->lpfnGet(
		hdDrawable,
		lpulPositionX,
		lpulPositionY,
		lpulWidth,
		lpulHeight
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
SetDrawableRotation(
	_In_		HDRAWABLE			hdDrawable,
	_In_ 		FLOAT 				fTheta,
	_In_ 		BOOL 				bOffset
){
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE lpDrawable;
	lpDrawable = ((DLPDRAWABLE)hdDrawable)[0];
	EXIT_IF_UNLIKELY_NULL(lpDrawable, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDrawable->lpfnSetTheta, FALSE);

	return lpDrawable->lpfnSetTheta(
		hdDrawable, 
		fTheta,
		bOffset
	);
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
GetDrawableRotation(
	_In_		HDRAWABLE			hdDrawable,
	_Out_ 		LPFLOAT				lpfTheta
){
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE lpDrawable;
	lpDrawable = ((DLPDRAWABLE)hdDrawable)[0];
	EXIT_IF_UNLIKELY_NULL(lpDrawable, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpDrawable->lpfnGetTheta, FALSE);

	return lpDrawable->lpfnGetTheta(
		hdDrawable, 
		lpfTheta
	);
}