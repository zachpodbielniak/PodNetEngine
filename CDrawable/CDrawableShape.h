#ifndef PODNETENGINE_DRAWABLESHAPE_H
#define PODNETENGINE_DRAWABLESHAPE_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"
#include "CDrawable.h"
#include "../CTexture/CTexture.h"
#include "../CRenderer/CRenderer.h"




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
HDRAWABLE
CreateDrawableShape(
	_In_ 		HANDLE 			hRenderer,
	_In_ 		SHAPE_TYPE 		stType,
	_In_ 		BOOL 			bFill
);


#endif