#ifndef PODNETENGINE_DRAWABLE_H
#define PODNETENGINE_DRAWABLE_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"



typedef enum __DRAWABLE_TYPE
{
	DRAWABLE_TYPE_IMAGE		= 1,
	DRAWABLE_TYPE_FONT		= 2,
	DRAWABLE_TYPE_SHAPE		= 3
} DRAWABLE_TYPE;



/* So We Can Put Type In Call Back */
typedef struct __DRAWABLE 	DRAWABLE, *LPDRAWABLE;
typedef LPVOID 			HDRAWABLE;



typedef _Call_Back_ BOOL(* LPFN_DRAWABLE_DRAW_PROC)(
	_In_ 		HDRAWABLE		hdDrawable,
	_In_Opt_ 	LPVOID 			lpParam
);


typedef _Call_Back_ BOOL(* LPFN_DRAWABLE_DESTROY_PROC)(
	_In_ 		HDRAWABLE		hdDrawable
);


typedef _Call_Back_ BOOL(* LPFN_DRAWABLE_SET_PROC)(
	_In_ 		HDRAWABLE		hdDrawable,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_ 		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight,
	_In_ 		BOOL 			bOffset
);


typedef _Call_Back_ BOOL(* LPFN_DRAWABLE_GET_PROC)(
	_In_ 		HDRAWABLE		hdDrawable,
	_Out_ 		LPULONG 		lpulPositionX,
	_Out_ 		LPULONG			lpulPositionY,
	_Out_ 		LPULONG			lpulWidth,
	_Out_ 		LPULONG 		lpulHeight
);


typedef _Call_Back_ BOOL(* LPFN_DRAWABLE_SET_THETA_PROC)(
	_In_ 		HDRAWABLE 		hdDrawable,
	_In_ 		FLOAT 			fTheta,
	_In_ 		BOOL 			bOffSet
);


typedef _Call_Back_ BOOL(* LPFN_DRAWABLE_GET_THETA_PROC)(
	_In_ 		HDRAWABLE 		hdDrawable,
	_Out_ 		LPFLOAT 		lpfTheta
);






typedef struct __DRAWABLE
{
	HANDLE 				hParentRenderer;
	LPVOID 				lpParent;
	LPFN_DRAWABLE_DRAW_PROC		lpfnDraw;
	LPFN_DRAWABLE_DESTROY_PROC	lpfnDestroy;
	LPFN_DRAWABLE_GET_PROC		lpfnGet;
	LPFN_DRAWABLE_SET_PROC		lpfnSet;
	LPFN_DRAWABLE_GET_THETA_PROC	lpfnGetTheta;
	LPFN_DRAWABLE_SET_THETA_PROC	lpfnSetTheta;
	DRAWABLE_TYPE			dtDrawType;
} DRAWABLE, *LPDRAWABLE, **DLPDRAWABLE;


#define INHERITS_FROM_DRAWABLE() 	\
	LPDRAWABLE 	lpDrawable;	\
	DRAWABLE_TYPE	dtType




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
InitializeDrawable(
	_In_ 		HANDLE 				hParentRenderer,
	_In_ 		LPDRAWABLE			lpThis,
	_In_ 		DRAWABLE_TYPE 			dtDrawType,
	_In_ 		LPVOID 				lpParent,
	_In_ 		LPFN_DRAWABLE_DRAW_PROC		lpfnDraw,
	_In_ 		LPFN_DRAWABLE_DESTROY_PROC	lpfnDestroy,
	_In_ 		LPFN_DRAWABLE_GET_PROC		lpfnGet,
	_In_ 		LPFN_DRAWABLE_SET_PROC		lpfnSet,
	_In_Opt_	LPFN_DRAWABLE_GET_THETA_PROC	lpfnGetTheta,
	_In_Opt_	LPFN_DRAWABLE_SET_THETA_PROC	lpfnSetTheta
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
Draw(
	_In_ 		HDRAWABLE			hdDrawable,
	_In_ 		LPVOID 				lpParam
);



_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
DestroyDrawable(
	_In_ 		HDRAWABLE			hdDrawable
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
SetDrawablePosition(
	_In_		HDRAWABLE			hdDrawable,
	_In_ 		ULONG 				ulPositionX,
	_In_ 		ULONG 				ulPositionY
);




#define SetDrawablePositionX(H, X)	\
	SetDrawablePosition((X), (X), MAX_ULONG)




#define SetDrawablePositionY(H, Y)	\
	SetDrawablePosition((H), MAX_ULONG, (Y))




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
SetDrawablePositionByOffset(
	_In_		HDRAWABLE			hdDrawable,
	_In_ 		ULONG 				ulPositionX,
	_In_ 		ULONG 				ulPositionY
);



_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
SetDrawableSize(
	_In_ 		HDRAWABLE 			hdDrawable,
	_In_ 		ULONG 				ulWidth,
	_In_ 		ULONG 				ulHeight
);




#define SetDrawableWidth(D, W)		\
	SetDrawableSize((D), (W), MAX_ULONG)




#define SetDrawableHeight(D, H)		\
	SetDrawableSize((D), MAX_ULONG, (H))




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
GetDrawablePosition(
	_In_		HDRAWABLE			hdDrawable,
	_Out_ 		LPULONG				lpulPositionX,
	_Out_ 		LPULONG				lpulPositionY
);




#define GetDrawablePositionX(D, X)	\
	GetDrawablePosition((D), &(X), NULL)




#define GetDrawablePositionY(D, Y)	\
	GetDrawablePosition((D), NULLPTR, &(Y))




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
GetDrawableSize(
	_In_		HDRAWABLE			hdDrawable,
	_Out_ 		LPULONG				lpulWidth,
	_Out_ 		LPULONG				lpulHeight
);




#define GetDrawableSizeWidth(D, W)	\
	GetDrawableSize((D), &(W), NULLPTR)




#define GetDrawableSizeHeight(D, H)	\
	GetDrawableSize((D), NULLPTR, &(H))




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
SetDrawablePositionAndSize(
	_In_		HDRAWABLE			hdDrawable,
	_In_ 		ULONG 				ulPositionX,
	_In_ 		ULONG 				ulPositionY,
	_In_ 		ULONG 				ulWidth,
	_In_ 		ULONG 				ulHeight
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
GetDrawablePositionAndSize(
	_In_		HDRAWABLE			hdDrawable,
	_Out_ 		LPULONG				lpulPositionX,
	_Out_ 		LPULONG				lpulPositionY,
	_Out_ 		LPULONG				lpulWidth,
	_Out_ 		LPULONG				lpulHeight
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
SetDrawableRotation(
	_In_		HDRAWABLE			hdDrawable,
	_In_ 		FLOAT 				fTheta,
	_In_ 		BOOL 				bOffset
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
GetDrawableRotation(
	_In_		HDRAWABLE			hdDrawable,
	_Out_ 		LPFLOAT				lpfTheta
);








#endif