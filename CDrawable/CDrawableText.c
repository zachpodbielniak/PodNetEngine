#include "CDrawableText.h"


typedef struct __DRAWABLE_TEXT
{
	INHERITS_FROM_DRAWABLE();
	LPTEXTURE		lptxTexture;
	FLOAT 			fTheta;
} DRAWABLE_TEXT, *LPDRAWABLE_TEXT;




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DrawText(
	_In_ 		HDRAWABLE		hdDrawable,
	_In_Opt_ 	LPVOID 			lpParam
){
	UNREFERENCED_PARAMETER(lpParam);
	EXIT_IF_UNLIKELY_NULL(hdDrawable, FALSE);

	LPDRAWABLE_TEXT lpText;
	lpText = (LPDRAWABLE_TEXT)hdDrawable;

	if (FALSE == FLOAT_EQUAL(0.0f, lpText->fTheta))
	{ 
		return AddTextureToRenderBufferAndRotate(
			lpText->lpDrawable->hParentRenderer,
			lpText->lptxTexture,
			lpText->fTheta
		);
	}

	return AddTextureToRenderBuffer(lpText->lpDrawable->hParentRenderer, lpText->lptxTexture);	
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyText(
	_In_ 		HDRAWABLE		hdDrawable
){
	LPDRAWABLE_TEXT lpText;
	lpText = (LPDRAWABLE_TEXT)hdDrawable;

	DestroyTexture(lpText->lptxTexture);
	FreeMemory(lpText);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__SetTextData(
	_In_ 		HDRAWABLE		hdDrawable,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_ 		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight,
	_In_ 		BOOL 			bOffset
){
	LPDRAWABLE_TEXT lpText;
	lpText = (LPDRAWABLE_TEXT)hdDrawable;

	switch (bOffset)
	{

		case TRUE:
		{
			if (MAX_ULONG != ulHeight)
			{ lpText->lptxTexture->wrectCoordinates.ulH += (LONG)ulHeight; }

			if (MAX_ULONG != ulWidth)
			{ lpText->lptxTexture->wrectCoordinates.ulW += (LONG)ulWidth; }

			if (MAX_ULONG != ulPositionX)
			{ lpText->lptxTexture->wrectCoordinates.ulX += (LONG)ulPositionX; }

			if (MAX_ULONG != ulPositionY)
			{ lpText->lptxTexture->wrectCoordinates.ulY += (LONG)ulPositionY; }
			break;
		}

		case FALSE:
		{
			if (MAX_ULONG != ulHeight)
			{ lpText->lptxTexture->wrectCoordinates.ulH = ulHeight; }

			if (MAX_ULONG != ulWidth)
			{ lpText->lptxTexture->wrectCoordinates.ulW = ulWidth; }

			if (MAX_ULONG != ulPositionX)
			{ lpText->lptxTexture->wrectCoordinates.ulX = ulPositionX; }

			if (MAX_ULONG != ulPositionY)
			{ lpText->lptxTexture->wrectCoordinates.ulY = ulPositionY; }
		}

	}

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__GetTextData(
	_In_ 		HDRAWABLE		hdDrawable,
	_Out_ 		LPULONG 		lpulPositionX,
	_Out_ 		LPULONG			lpulPositionY,
	_Out_ 		LPULONG			lpulWidth,
	_Out_ 		LPULONG 		lpulHeight
){
	LPDRAWABLE_TEXT lpText;
	lpText = (LPDRAWABLE_TEXT)hdDrawable;

	if (NULLPTR != lpulPositionX)
	{ *lpulPositionX = lpText->lptxTexture->wrectCoordinates.ulX; }

	if (NULLPTR != lpulPositionY)
	{ *lpulPositionY = lpText->lptxTexture->wrectCoordinates.ulY; }
	
	if (NULLPTR != lpulWidth)
	{ *lpulWidth = lpText->lptxTexture->wrectCoordinates.ulW; }

	if (NULLPTR != lpulHeight)
	{ *lpulHeight = lpText->lptxTexture->wrectCoordinates.ulH; }

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__SetTextRotation(
	_In_ 		HDRAWABLE		hdDrawable,
	_In_		FLOAT 			fTheta,
	_In_ 		BOOL 			bOffset
){
	LPDRAWABLE_TEXT lpText;
	lpText = (LPDRAWABLE_TEXT)hdDrawable;
	
	if (FALSE == bOffset)
	{ lpText->fTheta = fTheta; }
	else
	{ lpText->fTheta += fTheta; }

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__GetTextRotation(
	_In_ 		HDRAWABLE		hdDrawable,
	_In_Opt_	LPFLOAT 		lpfTheta
){
	LPDRAWABLE_TEXT lpText;
	lpText = (LPDRAWABLE_TEXT)hdDrawable;

	*lpfTheta = lpText->fTheta;
	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
HDRAWABLE
CreateDrawableText(
	_In_ 		HANDLE 			hRenderer,
	_In_Z_ 		LPSTR RESTRICT		lpszText,
	_In_Z_		LPSTR RESTRICT		lpszFontFile,
	_In_ 		ULONG			ulFontSize,
	_In_		COLOR			clrTextColor
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpszText, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpszFontFile, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, NULLPTR);

	if (0 == ulFontSize)
	{ ulFontSize = 12; }

	LPDRAWABLE_TEXT lpText;
	lpText = GlobalAllocAndZero(sizeof(DRAWABLE_TEXT));
	EXIT_IF_UNLIKELY_NULL(lpText, NULLPTR);

	lpText->dtType = DRAWABLE_TYPE_IMAGE;
	lpText->lpDrawable = GlobalAllocAndZero(sizeof(DRAWABLE));
	EXIT_IF_LIKELY_NULL(lpText->lpDrawable, NULLPTR);

	InitializeDrawable(
		hRenderer,
		lpText->lpDrawable,
		DRAWABLE_TYPE_FONT,
		lpText,
		__DrawText,
		__DestroyText,
		__GetTextData,
		__SetTextData,
		__GetTextRotation,
		__SetTextRotation
	);

	lpText->lptxTexture = CreateTextureFromText(
		hRenderer,
		lpszText,
		lpszFontFile,
		ulFontSize,
		clrTextColor
	);

	QueryTexture(lpText->lptxTexture);

	return lpText;
}