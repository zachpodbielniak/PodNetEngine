#ifndef PODNETENGINE_DRAWABLETEXT_H
#define PODNETENGINE_DRAWABLETEXT_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"
#include "CDrawable.h"
#include "../CTexture/CTexture.h"
#include "../CRenderer/CRenderer.h"




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
HDRAWABLE
CreateDrawableText(
	_In_ 		HANDLE 			hRenderer,
	_In_Z_ 		LPSTR RESTRICT		lpszText,
	_In_Z_		LPSTR RESTRICT		lpszFontFile,
	_In_ 		ULONG			ulFontSize,
	_In_		COLOR			clrTextColor
);




#endif