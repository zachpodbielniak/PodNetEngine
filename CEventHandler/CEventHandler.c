#include "CEventHandler.h"
#include "CEventHandlerObject.c"




_Success_(return != FALSE, _Acquires_Shared_Lock_())
INTERNAL_OPERATION
BOOL
__DestroyEventHandler(
	_In_ 		HDERIVATIVE 		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	UNREFERENCED_PARAMETER(hDerivative);

	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
CreateEventHandler(
	_In_ 		HANDLE 			hParentEngine
){
	HANDLE hEventHandler;
	LPEVENTHANDLER lpehHandler;
	lpehHandler = GlobalAllocAndZero(sizeof(EVENTHANDLER));
	EXIT_IF_UNLIKELY_NULL(lpehHandler, NULL_OBJECT);

	lpehHandler->hParentEngine = hParentEngine;
	lpehHandler->hLock = EngineGetInputLock(hParentEngine);

	lpehHandler->hEvent = CreateEvent(FALSE);
	SetEventPollTime(lpehHandler->hEvent, 1); 

	hEventHandler = CreateHandleWithSingleInheritor(
		lpehHandler,
		&(lpehHandler->hThis),
		HANDLE_TYPE_EVENT_HANDLER,
		__DestroyEventHandler,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpehHandler->ullId),
		0
	);

	return hEventHandler;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
HandleInput(
	_In_ 		HANDLE 			hEventHandler
){
	EXIT_IF_UNLIKELY_NULL(hEventHandler, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hEventHandler), HANDLE_TYPE_EVENT_HANDLER, FALSE);

	LPEVENTHANDLER lpehHandler;
	LPBYTE lpbyKeyStates;
	USHORT usIndex, usSize;
	BOOL bRet;

	lpehHandler = (LPEVENTHANDLER)GetHandleDerivative(hEventHandler);
	EXIT_IF_UNLIKELY_NULL(lpehHandler, FALSE);

	lpbyKeyStates = (LPBYTE)SDL_GetKeyboardState(NULLPTR);
	bRet = FALSE;

	WaitForSingleObject(lpehHandler->hLock, INFINITE_WAIT_TIME);

	while (0 != SDL_PollEvent(&(lpehHandler->ieEvents)))
	{

		/* Temporary */
		if (SDL_QUIT == lpehHandler->ieEvents.type)
		{ PostQuitMessage(0);}

	}

	for (
		usIndex = 0, usSize = KEY_LAST;
		usIndex < usSize;
		usIndex++
	){
		if (lpbyKeyStates[usIndex])
		{
			if (NULLPTR == lpehHandler->lpfnKeyDownHandlers[usIndex])
			{ continue; }

			bRet = lpehHandler->lpfnKeyDownHandlers[usIndex](
				lpehHandler->hParentEngine,
				hEventHandler,
				lpehHandler->dlpKeyDownHandlerParams[usIndex],
				usIndex
			);
		}
	}

	ReleaseSingleObject(lpehHandler->hLock);



	return bRet;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
RegisterEventKeyHandlerCallBack(
	_In_ 		HANDLE 					hEventHandler,
	_In_ 		KEY					kKey,
	_In_ 		LPFN_EVENT_INPUT_KEY_HANDLER_PROC	lpfnCallBack,
	_In_Opt_ 	LPVOID 					lpCallBackParam,
	_In_ 		BOOL 					bButtonPressed
){
	UNREFERENCED_PARAMETER(bButtonPressed); /* TODO: Add Support Later */

	EXIT_IF_UNLIKELY_NULL(hEventHandler, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hEventHandler), HANDLE_TYPE_EVENT_HANDLER, FALSE);

	LPEVENTHANDLER lpehHandler;
	lpehHandler = (LPEVENTHANDLER)GetHandleDerivative(hEventHandler);
	EXIT_IF_UNLIKELY_NULL(lpehHandler, FALSE);

	if (TRUE == bButtonPressed)
	{
		lpehHandler->lpfnKeyDownHandlers[kKey] = lpfnCallBack;
		lpehHandler->dlpKeyDownHandlerParams[kKey] = lpCallBackParam;
	}
	else
	{
		lpehHandler->lpfnKeyUpHandlers[kKey] = lpfnCallBack;
		lpehHandler->dlpKeyUpHandlerParams[kKey] = lpCallBackParam;
	}

	return TRUE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
RegisterEventMouseButtonHandlerCallBack(
	_In_ 		HANDLE 						hEventHandler,
	_In_ 		MOUSE_BUTTON					mbButton,
	_In_ 		LPFN_EVENT_INPUT_MOUSE_BUTTON_HANDLER_PROC	lpfnCallBack,
	_In_Opt_ 	LPVOID 						lpCallBackParam,
	_In_ 		BOOL 						bButtonPressed
);