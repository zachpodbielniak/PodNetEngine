#ifndef PODNETENGINE_EVENTHANDLEROBJECT_H
#define PODNETENGINE_EVENTHANDLEROBJECT_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"
#include "CKeys.h"
#include "CEventHandler.h"




typedef struct __EVENTHANDLER
{
	INHERITS_FROM_HANDLE();
	HANDLE 						hParentEngine;
	HANDLE						hLock;
	HANDLE 						hEvent;		/* Signal Its Done Processing Events */

	LPFN_EVENT_INPUT_KEY_HANDLER_PROC 		lpfnKeyDownHandlers[KEYS_TOTAL_AMOUNT];
	LPVOID 						dlpKeyDownHandlerParams[KEYS_TOTAL_AMOUNT];
	
	LPFN_EVENT_INPUT_KEY_HANDLER_PROC		lpfnKeyUpHandlers[KEYS_TOTAL_AMOUNT];
	LPVOID 						dlpKeyUpHandlerParams[KEYS_TOTAL_AMOUNT];
	
	LPFN_EVENT_INPUT_MOUSE_BUTTON_HANDLER_PROC	lpfnMouseButtonDownHandlers[5];
	LPVOID 						dlpMouseButtonDownHandlerParams[5];
	
	LPFN_EVENT_INPUT_MOUSE_BUTTON_HANDLER_PROC	lpfnMouseButtonUpHandlers[5];
	LPVOID 						dlpMouseButtonUpHandlerParams[5];

	INPUTEVENT				ieEvents;
} EVENTHANDLER, *LPEVENTHANDLER, **DLPEVENTHANDLER;


#endif