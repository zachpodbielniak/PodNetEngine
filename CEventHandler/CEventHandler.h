#ifndef PODNETENGINE_EVENTHANDLER_H
#define PODNETENGINE_EVENTHANDLER_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"
#include "CKeys.h"
#include "CMouseButtons.h"
#include "../CEngine/CEngine.h"


#ifndef EVENT_HANDLER_PROCS
#define EVENT_HANDLER_PROCS


typedef _Call_Back_ BOOL(* LPFN_EVENT_INPUT_KEY_HANDLER_PROC)(
	_In_ 		HANDLE 			hEngine,
	_In_ 		HANDLE 			hEventHandler,
	_In_Opt_	LPVOID 			lpParam,
	_In_ 		KEY 			kKeyPressed
);

typedef _Call_Back_ BOOL(* LPFN_EVENT_INPUT_MOUSE_BUTTON_HANDLER_PROC)(
	_In_ 		HANDLE 			hEngine,
	_In_ 		HANDLE 			hEventHandler,
	_In_Opt_ 	LPVOID 			lpParam,
	_In_ 		MOUSE_BUTTON		mbButton
);


#endif




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
CreateEventHandler(
	_In_ 		HANDLE 			hParentEngine
);




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
HandleInput(
	_In_ 		HANDLE 			hEventHandler
);




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
RegisterEventKeyHandlerCallBack(
	_In_ 		HANDLE 					hEventHandler,
	_In_ 		KEY					kKey,
	_In_ 		LPFN_EVENT_INPUT_KEY_HANDLER_PROC	lpfnCallBack,
	_In_Opt_ 	LPVOID 					lpCallBackParam,
	_In_ 		BOOL 					bButtonPressed
);




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
RegisterEventMouseButtonHandlerCallBack(
	_In_ 		HANDLE 						hEventHandler,
	_In_ 		MOUSE_BUTTON					mbButton,
	_In_ 		LPFN_EVENT_INPUT_MOUSE_BUTTON_HANDLER_PROC	lpfnCallBack,
	_In_Opt_ 	LPVOID 						lpCallBackParam,
	_In_ 		BOOL 						bButtonPressed
);







#endif