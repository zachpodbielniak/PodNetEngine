CC = gcc
STD = -std=c89
WARNINGS = -Wall -Wextra -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith
WARNINGS += -Wfloat-equal -Wswitch-enum -Wstrict-aliasing -Wno-missing-braces
WARNINGS += -Wno-cast-function-type #-Wno-switch-enum
DEFINES = -D _DEFAULT_SOURCE
DEFINES += -D _GNU_SOURCE

DEFINES_D = $(DEFINES)
DEFINES_D += -D __DEBUG__

OPTIMIZE = -Ofast -funroll-loops -fstrict-aliasing
OPTIMIZE += -fstack-protector-strong
MARCH = -march=native
MTUNE = -mtune=native

CC_FLAGS = $(STD)
CC_FLAGS += $(WARNINGS)
CC_FLAGS += $(DEFINES)
CC_FLAGS += $(OPTIMIZE)
CC_FLAGS += $(MARCH)
CC_FLAGS += $(MTUNE)

CC_FLAGS_D = $(STD)
CC_FLAGS_D += $(WARNINGS)
CC_FLAGS_D += $(DEFINES_D)

CC_FLAGS_T = $(CC_FLAGS_D)
CC_FLAGS_T += -Wno-unused-variable


FILES_DRAWABLE = CDrawable.o CDrawableImage.o CDrawableShape.o CDrawableText.o
FILES_ENGINE = CEngine.o
FILES_EVENT_HANDLER = CEventHandler.o
FILES_INIT = CInit.o
FILES_MESH = CMesh.o
FILES_RENDERER = CRenderer.o
FILES_SHADER = CShader.o
FILES_TEXTURE = CTexture.o
FILES_VERTEX = CVertex.o
FILES_WINDOW = CWindow.o 

FILES = $(FILES_DRAWABLE)
FILES += $(FILES_ENGINE)
FILES += $(FILES_EVENT_HANDLER)
FILES += $(FILES_INIT)
FILES += $(FILES_MESH)
FILES += $(FILES_RENDERER)
FILES += $(FILES_SHADER)
FILES += $(FILES_TEXTURE)
FILES += $(FILES_VERTEX)
FILES += $(FILES_WINDOW)




FILES_DRAWABLE_D = CDrawable_d.o CDrawableImage_d.o CDrawableShape_d.o CDrawableText_d.o
FILES_ENGINE_D = CEngine_d.o
FILES_EVENT_HANDLER_D = CEventHandler_d.o
FILES_INIT_D = CInit_d.o
FILES_MESH_D = CMesh_d.o
FILES_RENDERER_D = CRenderer_d.o 
FILES_SHADER_D = CShader_d.o
FILES_TEXTURE_D = CTexture_d.o
FILES_VERTEX_D = CVertex_d.o
FILES_WINDOW_D = CWindow_d.o 

FILES_D = $(FILES_DRAWABLE_D)
FILES_D += $(FILES_ENGINE_D)
FILES_D += $(FILES_EVENT_HANDLER_D)
FILES_D += $(FILES_INIT_D)
FILES_D += $(FILES_MESH_D)
FILES_D += $(FILES_RENDERER_D)
FILES_D += $(FILES_SHADER_D)
FILES_D += $(FILES_TEXTURE_D)
FILES_D += $(FILES_VERTEX_D)
FILES_D += $(FILES_WINDOW_D)




all: libpodnetengine.so 

debug: libpodnetengine_d.so

libpodnetengine.so: bin $(FILES)
	$(CC) -shared -fPIC -s -o bin/libpodnetengine.so bin/*.o -pthread -lpodnet -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_gfx -lGLEW -lGL $(STD) $(OPTIMIZE $(MARCH) 
	rm bin/*.o

libpodnetengine_d.so: bin $(FILES_D)
	$(CC) -g -shared -fPIC -o bin/libpodnetengine_d.so bin/*_d.o -pthread -lpodnet_d -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_gfx -lGLEW -lGL $(STD) 
	rm bin/*.o

bin: 
	mkdir -p bin

clean: 
	rm -rf bin

install:
	cp bin/libpodnetengine.so /usr/lib 

install_debug:
	cp bin/libpodnetengine_d.so /usr/lib

install_headers:
	mkdir -p /usr/include/PodNet/CEngine
	find . -type f -name "*.h" -exec install -D {} /usr/include/PodNet/CEngine/{} \;



CDrawable.o:
	$(CC) -fPIC -c -o bin/CDrawable.o CDrawable/CDrawable.c $(CC_FLAGS)

CDrawableImage.o:
	$(CC) -fPIC -c -o bin/CDrawableImage.o CDrawable/CDrawableImage.c $(CC_FLAGS)

CDrawableShape.o:
	$(CC) -fPIC -c -o bin/CDrawableShape.o CDrawable/CDrawableShape.c $(CC_FLAGS)

CDrawableText.o:
	$(CC) -fPIC -c -o bin/CDrawableText.o CDrawable/CDrawableText.c $(CC_FLAGS)

CEngine.o:
	$(CC) -fPIC -c -o bin/CEngine.o CEngine/CEngine.c $(CC_FLAGS)

CEventHandler.o:
	$(CC) -fPIC -c -o bin/CEventHandler.o CEventHandler/CEventHandler.c $(CC_FLAGS)

CInit.o:
	$(CC) -fPIC -c -o bin/CInit.o CInit/CInit.c $(CC_FLAGS)

CMesh.o:
	$(CC) -fPIC -c -o bin/CMesh.o CMesh/CMesh.c $(CC_FLAGS)

CRenderer.o:
	$(CC) -fPIC -c -o bin/CRenderer.o CRenderer/CRenderer.c $(CC_FLAGS)

CShader.o:
	$(CC) -fPIC -c -o bin/CShader.o CShader/CShader.c $(CC_FLAGS)

CTexture.o:
	$(CC) -fPIC -c -o bin/CTexture.o CTexture/CTexture.c $(CC_FLAGS)

CVertex.o:
	$(CC) -fPIC -c -o bin/CVertex.o CVertex/CVertex.c $(CC_FLAGS)

CWindow.o:
	$(CC) -fPIC -c -o bin/CWindow.o CWindow/CWindow.c $(CC_FLAGS)







CDrawable_d.o:
	$(CC) -g -fPIC -c -o bin/CDrawable_d.o CDrawable/CDrawable.c $(CC_FLAGS_D)

CDrawableImage_d.o:
	$(CC) -g -fPIC -c -o bin/CDrawableImage_d.o CDrawable/CDrawableImage.c $(CC_FLAGS_D)

CDrawableShape_d.o:
	$(CC) -g -fPIC -c -o bin/CDrawableShape_d.o CDrawable/CDrawableShape.c $(CC_FLAGS_D)

CDrawableText_d.o:
	$(CC) -g -fPIC -c -o bin/CDrawableText_d.o CDrawable/CDrawableText.c $(CC_FLAGS_D)

CEngine_d.o:
	$(CC) -g -fPIC -c -o bin/CEngine_d.o CEngine/CEngine.c $(CC_FLAGS_D)

CEventHandler_d.o:
	$(CC) -g -fPIC -c -o bin/CEventHandler_d.o CEventHandler/CEventHandler.c $(CC_FLAGS_D)

CInit_d.o:
	$(CC) -g -fPIC -c -o bin/CInit_d.o CInit/CInit.c $(CC_FLAGS_D)

CMesh_d.o:
	$(CC) -g -fPIC -c -o bin/CMesh_d.o CMesh/CMesh.c $(CC_FLAGS_D)

CRenderer_d.o:
	$(CC) -g -fPIC -c -o bin/CRenderer_d.o CRenderer/CRenderer.c $(CC_FLAGS_D)

CShader_d.o:
	$(CC) -g -fPIC -c -o bin/CShader_d.o CShader/CShader.c $(CC_FLAGS_D)

CTexture_d.o:
	$(CC) -g -fPIC -c -o bin/CTexture_d.o CTexture/CTexture.c $(CC_FLAGS_D)

CVertex_d.o:
	$(CC) -g -fPIC -c -o bin/CVertex_d.o CVertex/CVertex.c $(CC_FLAGS_D)

CWindow_d.o:
	$(CC) -g -fPIC -c -o bin/CWindow_d.o CWindow/CWindow.c $(CC_FLAGS_D)






CEngine_Test: bin
	$(CC) -g -fPIC -o bin/CEngine_Test Tests/Test_CEngine.c -lpodnet_d -lpodnetengine_d $(CC_FLAGS_T)

C2D_Test: bin
	$(CC) -g -fPIC -o bin/C2D_Test Tests/Test_C2D.c -lpodnet_d -lpodnetengine_d $(CC_FLAGS_T)

CDrawable_Test: bin
	$(CC) -g -fPIC -o bin/CDrawable_Test Tests/Test_CDrawable.c -lpodnet_d -lpodnetengine_d $(CC_FLAGS_T)

CShader_Test: bin
	$(CC) -g -fPIC -o bin/CShader_Test Tests/Test_CShader.c -lpodnet_d -lpodnetengine_d $(CC_FLAGS_T)



Pong:
	$(CC) -g -fPIC -o bin/Pong Examples/CPong.c -lpodnet_d -lpodnetengine_d $(CC_FLAGS_D)