#include "../CDrawable/CDrawable.h"
#include "../CDrawable/CDrawableImage.h"
#include "../CDrawable/CDrawableShape.h"
#include "../CDrawable/CDrawableText.h"
#include "../CEngine/CEngine.h"
#include "../CEventHandler/CEventHandler.h"
#include "../CRenderer/CRenderer.h"
#include "../CTexture/CTexture.h"
#include "../CWindow/CWindow.h"
#include <PodNet/CUnitTest/CUnitTest.h>




INTERNAL_OPERATION
BOOL
__KeyPressedEsc(
	_In_ 		HANDLE 		hEngine,
	_In_ 		HANDLE 		hEventHandler,
	_In_Opt_ 	LPVOID 		lpParam,
	_In_		KEY 		kKeyPressed	
){ 
	UNREFERENCED_PARAMETER(hEngine);
	UNREFERENCED_PARAMETER(hEventHandler);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(kKeyPressed);

	DieViolently("Killing The Program"); 
	return TRUE;
}




INTERNAL_OPERATION
BOOL
__KeyPressedMovement(
	_In_ 		HANDLE 		hEngine,
	_In_ 		HANDLE 		hEventHandler,
	_In_Opt_	LPVOID 		lpParam,
	_In_ 		KEY 		kKeyPressed
){
	UNREFERENCED_PARAMETER(hEngine);
	UNREFERENCED_PARAMETER(hEventHandler);

	HDRAWABLE hdDrawable;
	hdDrawable = (HDRAWABLE)lpParam;

	switch ((ULONG)kKeyPressed)
	{
		case (ULONG)KEY_W:
		{
			SetDrawablePositionByOffset(hdDrawable, 0, -5);
			break;
		}

		case (ULONG)KEY_S:
		{
			SetDrawablePositionByOffset(hdDrawable, 0, 5);
			break;
		}

		case (ULONG)KEY_A:
		{
			SetDrawablePositionByOffset(hdDrawable, -5, 0);
			break;
		}

		case (ULONG)KEY_D:
		{
			SetDrawablePositionByOffset(hdDrawable, 5, 0);
			break;
		}

		default: return FALSE;
	}

	return TRUE;
}




TEST_CALLBACK(__DrawableTest)
{
	TEST_BEGIN("Drawable Test");

	HANDLE hEngine, hWindow, hRenderer, hEventHandler;
	HDRAWABLE hdTest, hdCircle, hdRect;
	COLOR clrWhite;


	hEngine = CreateEngine(
		"Engine-Test",
		LOCK_TYPE_CRITICAL_SECTION,
		0x0C00,
		FALSE
	);

	hWindow = CreateRenderableWindow(
		"Window-Test",
		100,
		100,
		480,
		640,
		SDL_WINDOW_SHOWN
	);

	hRenderer = CreateRenderer(
		hWindow,
		SDL_RENDERER_ACCELERATED /*| SDL_RENDERER_PRESENTVSYNC */
	);

	hEventHandler = CreateEventHandler(hEngine);

	EngineSetMainRenderer(hEngine, hRenderer);
	EngineSetMainWindow(hEngine, hWindow);

	clrWhite.byRed = 255;
	clrWhite.byGreen = 255;
	clrWhite.byBlue = 255;
	clrWhite.byAlpha = 255;

	hdTest = CreateDrawableText(
		hRenderer,
		"This Is Inherited Text",
		"/usr/share/fonts/TTF/arial.ttf",
		16,
		clrWhite
	);

	SetDrawablePosition(hdTest, 30, 30);

	hdCircle = CreateDrawableShape(
		hRenderer,
		SHAPE_CIRCLE,
		TRUE
	);
	
	SetDrawablePositionAndSize(
		hdCircle,
		100,
		100,
		15,
		0
	);


	hdRect = CreateDrawableShape(
		hRenderer,
		SHAPE_RECTANGLE,
		TRUE
	);

	SetDrawablePositionAndSize(
		hdRect,
		10,
		10,
		20,
		70
	);


	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_ESCAPE,
		__KeyPressedEsc,
		NULLPTR,
		TRUE
	);
	
	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_W,
		__KeyPressedMovement,
		hdRect,
		TRUE
	);
	
	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_S,
		__KeyPressedMovement,
		hdRect,
		TRUE
	);
	
	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_A,
		__KeyPressedMovement,
		hdRect,
		TRUE
	);
	
	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_D,
		__KeyPressedMovement,
		hdRect,
		TRUE
	);





	INFINITE_LOOP()
	{
		SetRenderDrawColor(hRenderer, 0, 0, 255, 255);
		ClearRenderBuffer(hRenderer);

		Draw(hdTest, NULLPTR);
		Draw(hdCircle, NULLPTR);
		Draw(hdRect, NULLPTR);
		/*DrawCircle(hRenderer, 30, 400, 15, clrWhite);*/


		HandleInput(hEventHandler);

		Render(hRenderer);
		Sleep(15);
	}


	TEST_END();
}




TEST_START("CEngine");
TEST_ADD(__DrawableTest);
TEST_RUN();
TEST_CONCLUDE();