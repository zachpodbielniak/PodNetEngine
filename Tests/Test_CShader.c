#include "../PodNetEngine.h"
#include "../CEngine/CEngine.h"
#include "../CWindow/CWindow.h"
#include "../CShader/CShader.h"
#include "../CRenderer/CRenderer.h"
#include "../CMesh/CMesh.h"
#include <PodNet/CUnitTest/CUnitTest.h>


HANDLE hEngine, hWindow, hShader, hRenderer, hEventHandler, hMesh;
FLOAT fTheta = 0.0f;



_Call_Back_
_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__KeyPressedEsc(
	_In_ 		HANDLE 		hEngine,
	_In_ 		HANDLE 		hEventHandler,
	_In_Opt_ 	LPVOID 		lpParam,
	_In_		KEY 		kKeyPressed	
){ 
	UNREFERENCED_PARAMETER(hEngine);
	UNREFERENCED_PARAMETER(hEventHandler);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(kKeyPressed);

	PostQuitMessage(0);
	return TRUE;
}




_Call_Back_
_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__KeyPressedMovement(
	_In_ 		HANDLE 		hEngine,
	_In_ 		HANDLE 		hEventHandler,
	_In_Opt_	LPVOID		lpParam,
	_In_ 		KEY 		kKeyPressed
){
	UNREFERENCED_PARAMETER(hEngine);
	UNREFERENCED_PARAMETER(hEventHandler);
	UNREFERENCED_PARAMETER(lpParam);

	switch ((ULONG)kKeyPressed)
	{
		case (ULONG)KEY_A:
		{
			fTheta -= 0.01f;
			break;
		}

		case (ULONG)KEY_D:
		{
			fTheta += 0.01f;
			break;
		}
	}

	return TRUE;
}




_Call_Back_
_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__Update(
	_In_ 		HANDLE			hEngine,
	_In_Opt_ 	LPVOID 			lpParam
){
	UNREFERENCED_PARAMETER(hEngine);
	UNREFERENCED_PARAMETER(lpParam);

	HDRAWABLE hdFps, hdRect;
	COLOR clrWhite;

	clrWhite.ulValue = 0xFFFFFFFF;
	CSTRING csBuffer[0x20];
	ZeroMemory(csBuffer, sizeof(csBuffer));
	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"FPS: %.2f",
		EngineGetLastFps(hEngine)
	);

	hdFps = CreateDrawableText(
		hRenderer,
		csBuffer,
		"/usr/share/fonts/TTF/arial.ttf",
		18,
		clrWhite
	);

	SetDrawablePosition(
		hdFps,
		40,
		40
	);


	hdRect = CreateDrawableShape(
		hRenderer,
		SHAPE_RECTANGLE,
		FALSE
	);

	SetDrawablePositionAndSize(
		hdRect,
		400,
		500,
		60,
		60
	);

	SetDrawableRotation(
		hdRect,
		fTheta,
		FALSE
	);

	fTheta += 0.1f;

	HandleInput(hEventHandler);	

	SetRenderDrawColor(hRenderer, 0, 38, 77, 255);
	ClearRenderBuffer(hRenderer);

	Draw(hdFps, NULLPTR);
	ShaderBind(hShader);
	MeshDraw(hMesh);
	Draw(hdRect, NULLPTR);	

	DrawTriangle(
		hRenderer,
		200,
		200,
		60,
		60,
		PI_4,
		clrWhite,
		FALSE
	);

	Render(hRenderer);
	DestroyDrawable(hdFps);
	DestroyDrawable(hdRect);
	return TRUE;
}




TEST_CALLBACK(__CreateShader)
{
	TEST_BEGIN("CreateShader");

	HANDLE hEngine, hWindow, hShader;

	hEngine = CreateEngine(
		"Test",
		LOCK_TYPE_MUTEX,
		0,
		__Update,
		NULLPTR,
		FALSE
	);

	hWindow = CreateRenderableWindow(
		"Window",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		1280,
		720,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL
	);

	
	hShader = CreateShader("./Shaders/BasicShader");

	TEST_EXPECT_VALUE_NOT_EQUAL(hShader, NULL_OBJECT);

	TEST_END();
}


TEST_CALLBACK(__CreateMesh)
{
	TEST_BEGIN("CreateMesh");

	VERTEX vtxData[3];

	hEngine = CreateEngine(
		"Test",
		LOCK_TYPE_MUTEX,
		0,
		__Update,
		NULLPTR,
		FALSE
	);

	hWindow = CreateRenderableWindow(
		"Window",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		720,
		1280,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL
	);

	hRenderer = CreateRenderer(hWindow, SDL_RENDERER_ACCELERATED);
	hEventHandler = CreateEventHandler(hEngine);
	EngineSetMainRenderer(hEngine, hRenderer);
	EngineSetMainWindow(hEngine, hWindow);

	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_ESCAPE,
		__KeyPressedEsc,
		NULLPTR, 
		TRUE
	);

	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_A,
		__KeyPressedMovement,
		NULLPTR,
		TRUE
	);

	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_D,
		__KeyPressedMovement,
		NULLPTR,
		TRUE
	);

	hShader = CreateShader("./Shaders/BasicShader");
	TEST_EXPECT_VALUE_NOT_EQUAL(hShader, NULL_OBJECT);

	InitializeVertex(&(vtxData[0]), -0.5f, -0.5f, 0.0f);
	InitializeVertex(&(vtxData[1]), 0.0f, 0.5f, 0.0f);
	InitializeVertex(&(vtxData[2]), 0.5f, -0.5f, 0.0f);

	hMesh = CreateMesh(vtxData, 3, 0);

	EngineSetMaxFps(hEngine, 61);
	StartEngine(hEngine);

	TEST_END();
}







TEST_START("CShader");
TEST_ADD(__CreateMesh);
TEST_RUN();
TEST_CONCLUDE();
