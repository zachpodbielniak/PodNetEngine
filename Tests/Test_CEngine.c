#include "../CEngine/CEngine.h"
#include <PodNet/CUnitTest/CUnitTest.h>



TEST_CALLBACK(__CreateEngine)
{
	TEST_BEGIN("CreateEngine");

	HANDLE hEngine;
	hEngine = CreateEngine(
		"Engine-Test",
		LOCK_TYPE_CRITICAL_SECTION,
		0x0C00,
		FALSE
	);

	TEST_EXPECT_VALUE_NOT_EQUAL(hEngine, NULL_OBJECT);
	TEST_EXPECT_VALUE_NOT_EQUAL(DestroyObject(hEngine), FALSE);	

	TEST_END();
}


TEST_CALLBACK(__GetEngineLocks)
{
	TEST_BEGIN("GetEngineLocks");

	HANDLE hEngine;
	HANDLE hGlobalLock;
	HANDLE hRenderLock;

	hEngine = CreateEngine(
		"Engine-Test",
		LOCK_TYPE_CRITICAL_SECTION,
		0x0C00,
		FALSE
	);

	TEST_EXPECT_VALUE_NOT_EQUAL(hEngine, NULL_OBJECT);
	
	hGlobalLock = EngineGetGlobalLock(hEngine);
	hRenderLock = EngineGetRenderLock(hEngine);

	TEST_EXPECT_VALUE_NOT_EQUAL(hGlobalLock, NULL_OBJECT);
	TEST_EXPECT_VALUE_NOT_EQUAL(hRenderLock, NULL_OBJECT);	
	
	
	TEST_EXPECT_VALUE_NOT_EQUAL(DestroyObject(hEngine), FALSE);	


	TEST_END();
}





TEST_START("CEngine");
TEST_ADD(__CreateEngine);
TEST_ADD(__GetEngineLocks);
TEST_RUN();
TEST_CONCLUDE();
