#include "../CEngine/CEngine.h"
#include "../CEventHandler/CEventHandler.h"
#include "../CRenderer/CRenderer.h"
#include "../CTexture/CTexture.h"
#include "../CWindow/CWindow.h"
#include <PodNet/CUnitTest/CUnitTest.h>


INTERNAL_OPERATION
BOOL
__KeyPressedEsc(
	_In_ 		HANDLE 		hEngine,
	_In_ 		HANDLE 		hEventHandler,
	_In_Opt_ 	LPVOID 		lpParam,
	_In_		KEY 		kKeyPressed	
){ 
	UNREFERENCED_PARAMETER(hEngine);
	UNREFERENCED_PARAMETER(hEventHandler);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(kKeyPressed);

	DieViolently("Killing The Program"); 
	return TRUE;
}




INTERNAL_OPERATION
BOOL
__KeyPressedMovement(
	_In_ 		HANDLE 		hEngine,
	_In_ 		HANDLE 		hEventHandler,
	_In_Opt_	LPVOID 		lpParam,
	_In_ 		KEY 		kKeyPressed
){
	UNREFERENCED_PARAMETER(hEngine);
	UNREFERENCED_PARAMETER(hEventHandler);

	LPTEXTURE lptxFont;
	lptxFont = (LPTEXTURE)lpParam;

	switch ((ULONG)kKeyPressed)
	{
		case (ULONG)KEY_W:
		{
			UpdateTexturePositionByOffset(lptxFont, 0, -5);
			break;
		}

		case (ULONG)KEY_S:
		{
			UpdateTexturePositionByOffset(lptxFont, 0, 5);
			break;
		}

		case (ULONG)KEY_A:
		{
			UpdateTexturePositionByOffset(lptxFont, -5, 0);
			break;
		}

		case (ULONG)KEY_D:
		{
			UpdateTexturePositionByOffset(lptxFont, 5, 0);
			break;
		}

		default: return FALSE;
	}

	return TRUE;
}






TEST_CALLBACK(__2DTest)
{
	TEST_BEGIN("2D Test");

	HANDLE hEngine;
	HANDLE hEventHandler;
	HANDLE hRenderer;
	HANDLE hWindow;
	LPTEXTURE lptxTex, lptxFont;
	COLOR clrWhite;

	hEngine = CreateEngine(
		"Engine-Test",
		LOCK_TYPE_CRITICAL_SECTION,
		0x0C00,
		FALSE
	);

	hWindow = CreateRenderableWindow(
		"Window-Test",
		100,
		100,
		480,
		640,
		SDL_WINDOW_SHOWN
	);

	hRenderer = CreateRenderer(
		hWindow,
		SDL_RENDERER_ACCELERATED /*| SDL_RENDERER_PRESENTVSYNC */
	);

	hEventHandler = CreateEventHandler(hEngine);
	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_ESCAPE,
		__KeyPressedEsc,
		NULLPTR,
		TRUE
	);

	clrWhite.byRed = 255;
	clrWhite.byGreen = 255;
	clrWhite.byBlue = 255;
	clrWhite.byAlpha = 255;

	lptxTex = CreateTexture(hRenderer, "background.png", TEXTURE_FILE_PNG);
	lptxFont = CreateTextureFromText(
		hRenderer,
		"This Is A Font Texture",
		"/usr/share/fonts/TTF/arial.ttf",
		12,
		clrWhite
	);

	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_W,
		__KeyPressedMovement,
		lptxFont,
		TRUE
	);

	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_S,
		__KeyPressedMovement,
		lptxFont,
		TRUE
	);

	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_A,
		__KeyPressedMovement,
		lptxFont,
		TRUE
	);

	RegisterEventKeyHandlerCallBack(
		hEventHandler,
		KEY_D,
		__KeyPressedMovement,
		lptxFont,
		TRUE
	);

	lptxTex->wrectCoordinates.ulX = 100;
	lptxTex->wrectCoordinates.ulY = 100;

	QueryTexture(lptxTex);
	
	QueryTexture(lptxFont);
	UpdateTexturePosition(lptxFont, 50, 50);

	EngineSetMainRenderer(hEngine, hRenderer);
	EngineSetMainWindow(hEngine, hWindow);

	LPTEXTURE lptxFps;
	TIMESPEC tmSpec;			
	CSTRING csBuffer[0x100];
	ULONGLONG ullClock, ullTime;
	HighResolutionClock(&tmSpec);

	INFINITE_LOOP()
	{
		ZeroMemory(csBuffer, sizeof(csBuffer));
		ullClock = HighResolutionClockAndGetMilliSeconds(&tmSpec);		
		HighResolutionClock(&tmSpec);

		if (0 != ullClock)
		{ ullTime = 1000 / ullClock; }
		else
		{ ullTime = MAX_ULONGLONG; }

		StringPrintFormatSafe(
			csBuffer,
			sizeof(csBuffer) - 1,
			"FPS: %lu",
			ullTime
		);

		lptxFps = CreateTextureFromText(
			hRenderer,
			csBuffer,
			"/usr/share/fonts/TTF/arial.ttf",
			16,
			clrWhite
		);
		QueryTexture(lptxFps);
		UpdateTexturePosition(lptxFps, 400, 10);

		SetRenderDrawColor(hRenderer, 0, 0, 255, 255);
		ClearRenderBuffer(hRenderer);

		HandleInput(hEventHandler);

		AddTextureToRenderBuffer(hRenderer, lptxTex);
		UpdateTexturePosition(lptxTex, 300, 400);

		AddTextureToRenderBuffer(hRenderer, lptxFont);
		AddTextureToRenderBuffer(hRenderer, lptxFps);

		DrawCircle(hRenderer, 30, 400, 15, clrWhite);

		Render(hRenderer);

		DestroyTexture(lptxFps);

		Sleep(15);
	}

	Sleep(5000);

	TEST_END();
}




TEST_START("CEngine");
TEST_ADD(__2DTest);
TEST_RUN();
TEST_CONCLUDE();