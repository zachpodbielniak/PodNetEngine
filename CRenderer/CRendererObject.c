#ifndef PODNETENGINE_RENDEREROBJECT_C
#define PODNETENGINE_RENDEREROBJECT_C


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"


typedef struct __RENDERER
{
	INHERITS_FROM_HANDLE();
	HANDLE 		hLock;
	HANDLE 		hParentWindow;
	LPSDLRENDERER	lpsdlRenderer;
	ULONG 		ulFlags;
	ULONG 		ulWindowFlags;
} RENDERER, *LPRENDERER;



#endif