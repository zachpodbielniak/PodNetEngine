#ifndef PODNETENGINE_RENDERER_H
#define PODNETENGINE_RENDERER_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"
#include "../CTexture/CTexture.h"



_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
CreateRenderer(
	_In_ 		HANDLE 			hParentWindow,
	_In_Opt_ 	ULONG 			ulFlags
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
RendererGetParentWindow(
	_In_ 		HANDLE 			hRenderer
);




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
ClearRenderBuffer(
	_In_ 		HANDLE 			hRenderer
);




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
Render(
	_In_ 		HANDLE 			hRenderer
);



_Success_(return != FALSE, _Acquires_Lock_())
PODNETENGINE_API
BOOL
AddTextureToRenderBuffer(
	_In_		HANDLE 			hRenderer,
	_In_ 		LPTEXTURE 		lptxTexture
);




_Success_(return != FALSE, _Acquires_Lock_())
PODNETENGINE_API
BOOL
AddTextureToRenderBufferAndRotate(
	_In_		HANDLE 			hRenderer,
	_In_ 		LPTEXTURE 		lptxTexture,
	_In_ 		FLOAT 			fTheta
);




_Success_(return != FALSE, _Acquires_Lock_())
PODNETENGINE_API
BOOL
SetRenderDrawColor(
	_In_ 		HANDLE 			hRenderer,
	_In_ 		BYTE 			byRed,
	_In_ 		BYTE 			byGreen,
	_In_		BYTE 			byBlue,
	_In_ 		BYTE 			byAlpha
);




_Success_(return != FALSE, _Acquires_Lock_())
PODNETENGINE_API
BOOL
DrawCircle(
	_In_ 		HANDLE 			hRenderer,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_ 		ULONG 			ulRadius,
	_In_ 		COLOR 			clrCircle,
	_In_ 		BOOL 			bFill
);




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
DrawRectangle(
	_In_ 		HANDLE 			hRenderer,
	_In_		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_ 		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight,
	_In_ 		FLOAT 			fTheta,
	_In_ 		COLOR 			clrRectangle,
	_In_ 		BOOL 			bFill
);




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
DrawTriangle(
	_In_ 		HANDLE 			hRenderer,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight,
	_In_ 		FLOAT 			fTheta,
	_In_ 		COLOR 			clrTriangle,
	_In_ 		BOOL 			bFill
);



#endif