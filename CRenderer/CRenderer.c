#include "CRenderer.h"
#include "CRendererObject.c"
#include "../CWindow/CWindowObject.c"



_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyRenderer(
	_In_ 		HDERIVATIVE 	hDerivative,
	_In_Opt_ 	ULONG 		ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	UNREFERENCED_PARAMETER(hDerivative);
	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
CreateRenderer(
	_In_ 		HANDLE 		hParentWindow,
	_In_Opt_ 	ULONG 		ulFlags
){
	EXIT_IF_UNLIKELY_NULL(hParentWindow, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hParentWindow), HANDLE_TYPE_RENDERABLE_WINDOW, NULL_OBJECT);

	HANDLE hRenderer;
	LPRENDERER lpRenderer;
	LPRWINDOW lprWindow;

	IncrementObjectReferenceCount(hParentWindow);

	lprWindow = (LPRWINDOW)GetHandleDerivative(hParentWindow);
	EXIT_IF_UNLIKELY_NULL(lprWindow, NULL_OBJECT);

	lpRenderer = GlobalAllocAndZero(sizeof(RENDERER));
	EXIT_IF_UNLIKELY_NULL(lpRenderer, NULL_OBJECT);

	lpRenderer->hLock = CreateSpinLock();
	lpRenderer->ulFlags = ulFlags;
	lpRenderer->lpsdlRenderer = SDL_CreateRenderer(
		lprWindow->lpsdlWindow,
		-1,
		ulFlags
	);

	lpRenderer->ulWindowFlags = lprWindow->ulFlags;

	hRenderer = CreateHandleWithSingleInheritor(
		lpRenderer,
		&(lpRenderer->hThis),
		HANDLE_TYPE_RENDERER,
		__DestroyRenderer,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpRenderer->ullId),
		0
	);

	lprWindow->hRenderer = hRenderer;
	DecrementObjectReferenceCount(hParentWindow);

	return hRenderer;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
RendererGetParentWindow(
	_In_ 		HANDLE 		hRenderer
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, NULL_OBJECT);

	LPRENDERER lpRenderer;
	lpRenderer = (LPRENDERER)GetHandleDerivative(hRenderer);
	EXIT_IF_UNLIKELY_NULL(lpRenderer, NULL_OBJECT);

	return lpRenderer->hParentWindow;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
ClearRenderBuffer(
	_In_ 		HANDLE 		hRenderer
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, FALSE);

	LPRENDERER lpRenderer;
	lpRenderer = (LPRENDERER)GetHandleDerivative(hRenderer);
	EXIT_IF_UNLIKELY_NULL(lpRenderer, FALSE);

	if (0 != (lpRenderer->ulWindowFlags & SDL_WINDOW_OPENGL))
	{ glClear(GL_COLOR_BUFFER_BIT); }
	else
	{ SDL_RenderClear(lpRenderer->lpsdlRenderer); }

	return TRUE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
Render(
	_In_ 		HANDLE 		hRenderer
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, FALSE);

	LPRENDERER lpRenderer;
	lpRenderer = (LPRENDERER)GetHandleDerivative(hRenderer);
	EXIT_IF_UNLIKELY_NULL(lpRenderer, FALSE);

	SDL_RenderPresent(lpRenderer->lpsdlRenderer);
	return TRUE;
}




_Success_(return != FALSE, _Acquires_Lock_())
PODNETENGINE_API
BOOL
AddTextureToRenderBuffer(
	_In_		HANDLE 			hRenderer,
	_In_ 		LPTEXTURE 		lptxTexture
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, FALSE);
	EXIT_IF_UNLIKELY_NULL(lptxTexture, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, FALSE);

	LPRENDERER lpRenderer;
	lpRenderer = (LPRENDERER)GetHandleDerivative(hRenderer);
	EXIT_IF_UNLIKELY_NULL(lpRenderer, FALSE);

	SDL_RenderCopy(
		lpRenderer->lpsdlRenderer,
		lptxTexture->lpsdlTexture,
		NULLPTR,
		&(lptxTexture->wrectCoordinates.sdlRect)
	);


	return TRUE;
}




_Success_(return != FALSE, _Acquires_Lock_())
PODNETENGINE_API
BOOL
AddTextureToRenderBufferAndRotate(
	_In_		HANDLE 			hRenderer,
	_In_ 		LPTEXTURE 		lptxTexture,
	_In_ 		FLOAT 			fTheta
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, FALSE);
	EXIT_IF_UNLIKELY_NULL(lptxTexture, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, FALSE);

	LPRENDERER lpRenderer;
	lpRenderer = (LPRENDERER)GetHandleDerivative(hRenderer);
	EXIT_IF_UNLIKELY_NULL(lpRenderer, FALSE);

	SDL_RenderCopyEx(
		lpRenderer->lpsdlRenderer,
		lptxTexture->lpsdlTexture,
		NULLPTR,
		&(lptxTexture->wrectCoordinates.sdlRect),
		(DOUBLE)RADIANS_TO_DEGREES(fTheta),
		NULLPTR,
		SDL_FLIP_NONE
	);

	return TRUE;
}




_Success_(return != FALSE, _Acquires_Lock_())
PODNETENGINE_API
BOOL
SetRenderDrawColor(
	_In_ 		HANDLE 			hRenderer,
	_In_ 		BYTE 			byRed,
	_In_ 		BYTE 			byGreen,
	_In_		BYTE 			byBlue,
	_In_ 		BYTE 			byAlpha
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, FALSE);

	LPRENDERER lpRenderer;
	lpRenderer = (LPRENDERER)GetHandleDerivative(hRenderer);
	EXIT_IF_UNLIKELY_NULL(lpRenderer, FALSE);

	
	if (0 != (lpRenderer->ulWindowFlags & SDL_WINDOW_OPENGL))
	{	
		glClearColor(
			(FLOAT)(byRed / 255.0f),
			(FLOAT)(byGreen / 255.0f),
			(FLOAT)(byBlue / 255.0f),
			(FLOAT)(byAlpha / 255.0f)
		);
	}
	else
	{
		SDL_SetRenderDrawColor(
			lpRenderer->lpsdlRenderer,
			byRed,
			byGreen,
			byBlue,
			byAlpha
		);
	}

	return TRUE;
}




_Success_(return != FALSE, _Acquires_Lock_())
PODNETENGINE_API
BOOL
DrawCircle(
	_In_ 		HANDLE 			hRenderer,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_ 		ULONG 			ulRadius,
	_In_ 		COLOR 			clrCircle,
	_In_ 		BOOL 			bFill
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, FALSE);

	LPRENDERER lpRenderer;
	lpRenderer = (LPRENDERER)GetHandleDerivative(hRenderer);
	EXIT_IF_UNLIKELY_NULL(lpRenderer, FALSE);

	if (FALSE == bFill)
	{
		circleColor(
			lpRenderer->lpsdlRenderer,
			(SHORT)ulPositionX,
			(SHORT)ulPositionY,
			(SHORT)ulRadius,
			clrCircle.ulValue
		);
	}
	else
	{
		filledCircleColor(
			lpRenderer->lpsdlRenderer,
			(SHORT)ulPositionX,
			(SHORT)ulPositionY,
			(SHORT)ulRadius,
			clrCircle.ulValue
		);
	}

	return TRUE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
DrawRectangle(
	_In_ 		HANDLE 			hRenderer,
	_In_		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_ 		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight,
	_In_ 		FLOAT 			fTheta,
	_In_ 		COLOR 			clrRectangle,
	_In_ 		BOOL 			bFill
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, FALSE);

	LPRENDERER lpRenderer;
	LONG lX1, lX2, lY1, lY2;
	ARCHLONG alIndex;
	COLOR clrCurrent;
	VECTOR2D vec2P1, vec2P2, vec2P3, vec2P4;
	VECTOR2D vec2R1, vec2R2, vec2R3, vec2R4;
	lpRenderer = (LPRENDERER)GetHandleDerivative(hRenderer);
	EXIT_IF_UNLIKELY_NULL(lpRenderer, FALSE);

	if (FLOAT_EQUAL(0.0f, fTheta))
	{
		lX1 = ((LONG)ulPositionX + (LONG)(ulWidth / 2));
		lX2 = ((LONG)ulPositionX - (LONG)(ulWidth / 2));
		lY1 = ((LONG)ulPositionY - (LONG)(ulHeight / 2));
		lY2 = lY1;
	}
	else
	{
		/* Top Right */
		vec2P1.fX = (FLOAT)((LONG)ulPositionX + (LONG)ulWidth / 2) - (LONG)ulPositionX;
		vec2P1.fY = (FLOAT)((LONG)ulPositionY - (LONG)ulHeight / 2) - (LONG)ulPositionY;

		/* Top Left */
		vec2P2.fX = (FLOAT)((LONG)ulPositionX - (LONG)ulWidth / 2) - (LONG)ulPositionX;
		vec2P2.fY = (FLOAT)((LONG)ulPositionY - (LONG)ulHeight /2 ) - (LONG)ulPositionY;
		
		/* Bottom Right */
		vec2P3.fX = (FLOAT)((LONG)ulPositionX + (LONG)ulWidth / 2) - (LONG)ulPositionX;
		vec2P3.fY = (FLOAT)((LONG)ulPositionY + (LONG)ulHeight / 2) - (LONG)ulPositionY;

		/* Bottom Left */
		vec2P4.fX = (FLOAT)((LONG)ulPositionX - (LONG)ulWidth / 2) - (LONG)ulPositionX;
		vec2P4.fY = (FLOAT)((LONG)ulPositionY + (LONG)ulHeight /2 ) - (LONG)ulPositionY;

		/* Apply Rotation */

		vec2R1.fX = (vec2P1.fX * cosf(fTheta)) - (vec2P1.fY * sinf(fTheta)) + (LONG)ulPositionX;
		vec2R1.fY = (vec2P1.fX * sinf(fTheta)) + (vec2P1.fY * cosf(fTheta)) + (LONG)ulPositionY;
		
		vec2R2.fX = (vec2P2.fX * cosf(fTheta)) - (vec2P2.fY * sinf(fTheta)) + (LONG)ulPositionX;
		vec2R2.fY = (vec2P2.fX * sinf(fTheta)) + (vec2P2.fY * cosf(fTheta)) + (LONG)ulPositionY;
		
		vec2R3.fX = (vec2P3.fX * cosf(fTheta)) - (vec2P3.fY * sinf(fTheta)) + (LONG)ulPositionX;
		vec2R3.fY = (vec2P3.fX * sinf(fTheta)) + (vec2P3.fY * cosf(fTheta)) + (LONG)ulPositionY;
		
		vec2R4.fX = (vec2P4.fX * cosf(fTheta)) - (vec2P4.fY * sinf(fTheta)) + (LONG)ulPositionX;
		vec2R4.fY = (vec2P4.fX * sinf(fTheta)) + (vec2P4.fY * cosf(fTheta)) + (LONG)ulPositionY;
	}


	SDL_GetRenderDrawColor(
		lpRenderer->lpsdlRenderer,
		&clrCurrent.byRed,
		&clrCurrent.byGreen,
		&clrCurrent.byBlue,
		&clrCurrent.byAlpha
	);

	

	SDL_SetRenderDrawColor(
		lpRenderer->lpsdlRenderer,
		clrRectangle.byRed,
		clrRectangle.byGreen,
		clrRectangle.byBlue,
		clrRectangle.byAlpha
	);

	if (FLOAT_EQUAL(0.0f, fTheta))
	{
		if (FALSE == bFill)
		{
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, lX1, lY1, lX2, lY2);
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, lX1, lY1, lX1, lY2 + ulHeight);
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, lX2, lY1, lX2, lY2 + ulHeight);
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, lX1, lY1 + ulHeight, lX2, lY2 + ulHeight);
		}
		else
		{
			for (
				alIndex = 0;
				alIndex <= ulHeight;
				alIndex++
			){ SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, lX1, lY1 + alIndex, lX2, lY2 + alIndex); }			
		}
	}
	else 
	{
		if (FALSE == bFill)
		{
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, (LONG)vec2R1.fX, (LONG)vec2R1.fY, (LONG)vec2R2.fX, (LONG)vec2R2.fY);
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, (LONG)vec2R2.fX, (LONG)vec2R2.fY, (LONG)vec2R4.fX, (LONG)vec2R4.fY);
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, (LONG)vec2R3.fX, (LONG)vec2R3.fY, (LONG)vec2R4.fX, (LONG)vec2R4.fY);
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, (LONG)vec2R3.fX, (LONG)vec2R3.fY, (LONG)vec2R1.fX, (LONG)vec2R1.fY);
		}
		else
		{
			for (
				alIndex = 0;
				alIndex <= ulHeight;
				alIndex++
			){ 
				VECTOR2D vec2NewP1, vec2NewP2;
				VECTOR2D vec2NewR1, vec2NewR2;

				/* Top Right */
				vec2NewP1.fX = (FLOAT)((LONG)ulPositionX + (LONG)ulWidth / 2) - (LONG)ulPositionX;
				vec2NewP1.fY = (FLOAT)((LONG)ulPositionY - ((LONG)ulHeight / 2) + alIndex) - (LONG)ulPositionY;

				/* Top Left */
				vec2NewP2.fX = (FLOAT)((LONG)ulPositionX - (LONG)ulWidth / 2) - (LONG)ulPositionX;
				vec2NewP2.fY = (FLOAT)((LONG)ulPositionY - ((LONG)ulHeight / 2) + alIndex) - (LONG)ulPositionY;

				/* Apply Rotation */
				vec2NewR1.fX = (vec2NewP1.fX * cosf(fTheta)) - (vec2NewP1.fY * sinf(fTheta)) + (LONG)ulPositionX;
				vec2NewR1.fY = (vec2NewP1.fX * sinf(fTheta)) + (vec2NewP1.fY * cosf(fTheta)) + (LONG)ulPositionY;
		
				vec2NewR2.fX = (vec2NewP2.fX * cosf(fTheta)) - (vec2NewP2.fY * sinf(fTheta)) + (LONG)ulPositionX;
				vec2NewR2.fY = (vec2NewP2.fX * sinf(fTheta)) + (vec2NewP2.fY * cosf(fTheta)) + (LONG)ulPositionY;



				SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, (LONG)vec2NewR1.fX, (LONG)vec2NewR1.fY, (LONG)vec2NewR2.fX, (LONG)vec2NewR2.fY); 
			}
		}

	}


	SDL_SetRenderDrawColor(
		lpRenderer->lpsdlRenderer,
		clrCurrent.byRed,
		clrCurrent.byGreen,
		clrCurrent.byBlue,
		clrCurrent.byAlpha
	);

	return TRUE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
DrawTriangle(
	_In_ 		HANDLE 			hRenderer,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight,
	_In_ 		FLOAT 			fTheta,
	_In_ 		COLOR 			clrTriangle,
	_In_ 		BOOL 			bFill
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, FALSE);

	LPRENDERER lpRenderer;
	ARCHLONG alIndex;
	COLOR clrCurrent;
	VECTOR2D vec2P1, vec2P2, vec2P3;
	VECTOR2D vec2R1, vec2R2, vec2R3;
	lpRenderer = (LPRENDERER)GetHandleDerivative(hRenderer);
	EXIT_IF_UNLIKELY_NULL(lpRenderer, FALSE);

	SDL_GetRenderDrawColor(
		lpRenderer->lpsdlRenderer,
		&(clrCurrent.byRed),
		&(clrCurrent.byGreen),
		&(clrCurrent.byBlue),
		&(clrCurrent.byAlpha)
	);

	SDL_SetRenderDrawColor(
		lpRenderer->lpsdlRenderer,
		clrTriangle.byRed,
		clrTriangle.byGreen,
		clrTriangle.byBlue,
		clrTriangle.byAlpha
	);
	
	
	if (FALSE == bFill)
	{
		/* Rotated Non-Filled */
		if (FALSE == FLOAT_EQUAL(0.0f, fTheta))
		{
			/* Top */
			vec2P1.fX = (FLOAT)((LONG)ulPositionX);
			vec2P1.fY = (FLOAT)((LONG)ulPositionY - (ulHeight / 2));

			/* Bottom Right */
			vec2P2.fX = (FLOAT)((LONG)ulPositionX + (LONG)(ulWidth / 2));
			vec2P2.fY = (FLOAT)((LONG)ulPositionY + (LONG)(ulHeight / 2));

			/* Bottom Left */
			vec2P3.fX = (FLOAT)((LONG)ulPositionX - (LONG)(ulWidth / 2));
			vec2P3.fY = (FLOAT)((LONG)ulPositionY + (LONG)(ulHeight / 2));

			/* Apply Rotation */
			vec2R1.fX = (vec2P1.fX * cosf(fTheta)) - (vec2P1.fY * sinf(fTheta)) + (LONG)ulPositionX;
			vec2R1.fY = (vec2P1.fX * sinf(fTheta)) + (vec2P1.fY * cosf(fTheta)) + (LONG)ulPositionY;

			vec2R2.fX = (vec2P2.fX * cosf(fTheta)) - (vec2P2.fY * sinf(fTheta)) + (LONG)ulPositionX;
			vec2R2.fY = (vec2P2.fX * sinf(fTheta)) + (vec2P2.fY * cosf(fTheta)) + (LONG)ulPositionY;
		
			vec2R3.fX = (vec2P3.fX * cosf(fTheta)) - (vec2P3.fY * sinf(fTheta)) + (LONG)ulPositionX;
			vec2R3.fY = (vec2P3.fX * sinf(fTheta)) + (vec2P3.fY * cosf(fTheta)) + (LONG)ulPositionY;
			
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, vec2R1.fX, vec2R1.fY, vec2R2.fX, vec2R2.fY);
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, vec2R2.fX, vec2R2.fY, vec2R3.fX, vec2R3.fY);
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, vec2R3.fX, vec2R3.fY, vec2R1.fX, vec2R1.fY);
		}
		/* Rotated Filled */
		else
		{
			/* Top */
			vec2P1.fX = (FLOAT)((LONG)ulPositionX);
			vec2P1.fY = (FLOAT)((LONG)ulPositionY - (ulHeight / 2));

			/* Bottom Right */
			vec2P2.fX = (FLOAT)((LONG)ulPositionX + (LONG)(ulWidth / 2));
			vec2P2.fY = (FLOAT)((LONG)ulPositionY + (LONG)(ulHeight / 2));

			/* Bottom Left */
			vec2P3.fX = (FLOAT)((LONG)ulPositionX - (LONG)(ulWidth / 2));
			vec2P3.fY = (FLOAT)((LONG)ulPositionY + (LONG)(ulHeight / 2));

			/* Draw It */
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, vec2P1.fX, vec2P1.fY, vec2P2.fX, vec2P2.fY);
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, vec2P2.fX, vec2P2.fY, vec2P3.fX, vec2P3.fY);
			SDL_RenderDrawLine(lpRenderer->lpsdlRenderer, vec2P3.fX, vec2P3.fY, vec2P1.fX, vec2P1.fY);
		}
	}
	else
	{

	}	







	
	SDL_SetRenderDrawColor(
		lpRenderer->lpsdlRenderer,
		clrCurrent.byRed,
		clrCurrent.byGreen,
		clrCurrent.byBlue,
		clrCurrent.byAlpha
	);

	return TRUE;
}