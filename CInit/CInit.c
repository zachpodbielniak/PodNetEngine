#include "CInit.h"


GLOBAL_VARIABLE HANDLE 	hLock = NULL_OBJECT;
GLOBAL_VARIABLE BOOL 	bInitSdl = FALSE;
GLOBAL_VARIABLE BOOL 	bInitSdlImg = FALSE;
GLOBAL_VARIABLE BOOL 	bInitSdlTtf = FALSE;
GLOBAL_VARIABLE BOOL 	bInitOpenGl = FALSE;
GLOBAL_VARIABLE BOOL 	bInitGlew = FALSE;




_Success_(return != FALSE, _Interlocked_Operation_)
PODNETENGINE_API
BOOL
InitializePodNetEngine(
	VOID
){
	UNREFERENCED_PARAMETER(bInitOpenGl);

	HANDLE hLocal;
	BOOL bTest;
	hLocal = InterlockedAcquire(&hLock);

	if (NULL_OBJECT == hLocal)
	{
		hLocal = CreateSpinLock();

		bTest = InterlockedAcquireExchangeHappened(&hLock, hLocal);

		if (FALSE == bTest)
		{ DestroyObject(hLocal); }
	}

	WaitForSingleObject(hLock, INFINITE_WAIT_TIME);

	if (FALSE == bInitSdl)
	{ 
		SDL_Init(SDL_INIT_EVERYTHING); 
		bInitSdl = TRUE;
	}

	if (FALSE == bInitSdlImg)
	{
		IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG | IMG_INIT_TIF | IMG_INIT_WEBP);
		bInitSdlImg = TRUE;
	}

	if (FALSE == bInitSdlTtf)
	{
		TTF_Init();
		bInitSdlTtf = TRUE;
	}

	if (FALSE == bInitGlew)
	{
		bInitGlew = TRUE;
	}

	ReleaseSingleObject(hLock);

	return PodNetEngineHasBeenInitialized();
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNETENGINE_API
BOOL
PodNetEngineHasBeenInitialized(
	VOID
){
	BOOL bRet;
	bRet = TRUE;

	if (NULL_OBJECT == InterlockedAcquire(&hLock))
	{ return FALSE; }

	WaitForSingleObject(hLock, INFINITE_WAIT_TIME);

	bRet &= bInitSdl;
	bRet &= bInitSdlImg;
	bRet &= bInitSdlTtf;
	bRet &= bInitGlew;	

	ReleaseSingleObject(hLock);

	return bRet;
}