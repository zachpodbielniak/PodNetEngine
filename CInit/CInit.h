#ifndef PODNETENGINE_INIT_H
#define PODNETENGINE_INIT_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"



_Success_(return != FALSE, _Interlocked_Operation_)
PODNETENGINE_API
BOOL
InitializePodNetEngine(
	VOID
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNETENGINE_API
BOOL
PodNetEngineHasBeenInitialized(
	VOID
);




#endif