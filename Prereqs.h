#ifndef PODNETENGINE_PREREQS_H
#define PODNETENGINE_PREREQS_H


#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL2_gfxPrimitives.h>
#include <GL/glew.h>



#include <PodNet/PodNet.h>
#include <PodNet/CLock/CLock.h>
#include <PodNet/CContainers/CContainers.h>
#include <PodNet/CThread/CThread.h>
#include <PodNet/CMath/CMath.h>
#include <PodNet/CMath/CVector2D.h>
#include <PodNet/CMemory/CMemory.h>
#include <PodNet/CFile/CFile.h>






#endif