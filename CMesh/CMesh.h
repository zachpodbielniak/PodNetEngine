#ifndef PODNETENGINE_MESH_H
#define PODNETENGINE_MESH_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"
#include "../CVertex/CVertex.h"


typedef enum __MESH_FLAGS
{
	MESH_FLAG_DESTROY_VERTICES_ON_OBJECT_DESTRUCTION	= 1 << 0
} MESH_FLAGS;



_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
CreateMesh(
	_In_ 		LPVERTEX		lpvtxVertices,
	_In_ 		UARCHLONG		ualNumberOfVertices,
	_In_ 		ULONG 			ulFlags	
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
BOOL
MeshDraw(
	_In_		HANDLE 			hMesh
);




#endif