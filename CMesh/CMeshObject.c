#ifndef PODNETENGINE_MESHOBJECT_C
#define PODNETENGINE_MESHOBJECT_C


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"
#include "../CVertex/CVertex.h"


typedef enum __BUFFER_TYPES
{
	BUFFER_TYPE_POSITION_VB, 

	NUM_BUFFERS
} BUFFER_TYPES;



typedef struct __MESH 
{
	INHERITS_FROM_HANDLE();
	LPVERTEX		lpvtxVertices;
	UARCHLONG 		ualNumberOfVertices;
	ULONG 			ulVertexArrayObject;
	ULONG 			lpulVertexArrayBuffers[NUM_BUFFERS];
	ULONG 			ulDrawCount;
	ULONG 			ulFlags;
} MESH, *LPMESH;



#endif