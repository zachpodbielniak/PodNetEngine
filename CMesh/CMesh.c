#include "CMesh.h"
#include "CMeshObject.c"




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyMesh(
	_In_ 		HDERIVATIVE 		hDerivative,
	_In_ 		ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(ulFlags, FALSE);

	LPMESH lpMesh;
	lpMesh = (LPMESH)hDerivative;

	glDeleteVertexArrays(1, &(lpMesh->ulVertexArrayObject));

	if (0 != (lpMesh->ulFlags & MESH_FLAG_DESTROY_VERTICES_ON_OBJECT_DESTRUCTION))
	{
		UARCHLONG ualIndex;
		for (
			ualIndex = 0;
			ualIndex < lpMesh->ualNumberOfVertices;
			ualIndex++
		){ DestroyVertex((LPVOID)((UARCHLONG)lpMesh->lpvtxVertices + sizeof(VERTEX))); }
	}



	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
CreateMesh(
	_In_ 		LPVERTEX		lpvtxVertices,
	_In_ 		UARCHLONG		ualNumberOfVertices,
	_In_Opt_ 	ULONG 			ulFlags
){
	EXIT_IF_UNLIKELY_NULL(lpvtxVertices, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NULL(ualNumberOfVertices, NULL_OBJECT);

	HANDLE hMesh;
	LPMESH lpMesh;
	lpMesh = GlobalAllocAndZero(sizeof(MESH));

	lpMesh->ulDrawCount = ualNumberOfVertices;
	lpMesh->lpvtxVertices = lpvtxVertices;
	lpMesh->ulFlags = ulFlags;

	glGenVertexArrays(1, &(lpMesh->ulVertexArrayObject));
	glBindVertexArray(lpMesh->ulVertexArrayObject);

	glGenBuffers(NUM_BUFFERS, lpMesh->lpulVertexArrayBuffers);
	glBindBuffer(GL_ARRAY_BUFFER, lpMesh->lpulVertexArrayBuffers[BUFFER_TYPE_POSITION_VB]);
	glBufferData(GL_ARRAY_BUFFER, ualNumberOfVertices * sizeof(VERTEX), lpvtxVertices, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, FALSE, 0, 0);

	glBindVertexArray(0); /* Unbind */

	hMesh = CreateHandleWithSingleInheritor(
		lpMesh,
		&(lpMesh->hThis),
		HANDLE_TYPE_MESH,
		__DestroyMesh,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpMesh->ullId),
		0
	);

	return hMesh;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
BOOL
MeshDraw(
	_In_		HANDLE 			hMesh
){
	EXIT_IF_UNLIKELY_NULL(hMesh, FALSE);
	EXIT_IF_NOT_VALUE(GetHandleDerivativeType(hMesh), HANDLE_TYPE_MESH, FALSE);

	LPMESH lpMesh;
	lpMesh = GetHandleDerivative(hMesh);
	EXIT_IF_UNLIKELY_NULL(lpMesh, FALSE);

	glBindVertexArray(lpMesh->ulVertexArrayObject);

	glDrawArrays(GL_TRIANGLES, 0, lpMesh->ulDrawCount);

	glBindVertexArray(0);
	return TRUE;
}