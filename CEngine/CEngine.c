#include "CEngine.h"
#include "CEngineObject.c"


#define ENGINE_VECTOR_SIZE		0x10







_Success_(return != FALSE, _Acquires_Shared_Lock_())
INTERNAL_OPERATION
BOOL
__DestroyEngine(
	_In_ 		HDERIVATIVE		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPENGINE lpEngine;
	lpEngine = (LPENGINE)hDerivative;

	if (NULL_OBJECT != lpEngine->hGlobalLock)
	{
		WaitForSingleObject(lpEngine->hGlobalLock, INFINITE_WAIT_TIME);
		DestroyObject(lpEngine->hGlobalLock);
	}

	FreeMemory(lpEngine);

	return TRUE;
}




_Thread_Proc_
_Success_(return != FALSE, _Acquires_Shared_Lock_())
INTERNAL_OPERATION
LPVOID
__EngineLoop(
	_In_ 		LPVOID			lpParam
){
	if (NULLPTR == lpParam)
	{ DieViolently("Engine Loop Was Not Provided With A HANDLE To Itself"); }

	LPENGINE lpEngine;
	HANDLE hStart;
	TIMESPEC tmSpec;
	ULONGLONG ullClock;

	lpEngine = (LPENGINE)lpParam;
	hStart = lpEngine->hStart;



	while (FALSE != lpEngine->bRunning)
	{
		WaitForSingleObject(hStart, INFINITE_WAIT_TIME);
		HighResolutionClock(&tmSpec);
		RenderableWindowSwapBuffer(*(LPHANDLE)VectorAt(lpEngine->hvWindows, 0));
		
		lpEngine->lpfnUpdate(lpEngine->hThis, lpEngine->lpUpdateParam);

		ullClock = HighResolutionClockAndGetMicroSeconds(&tmSpec);
		if (
			((1000000 / ullClock) >= lpEngine->ulGoalFps) &&
			0 != lpEngine->ulGoalFps
		){ MicroSleep((1000000 / lpEngine->ulGoalFps) - ullClock); }


		ullClock = HighResolutionClockAndGetMicroSeconds(&tmSpec);
		lpEngine->fLastFrameFps = (FLOAT)(1000000.0f / ullClock);
	}

	return NULLPTR;
}




_Success_(return != FALSE, _Interlocked_)
INTERNAL_OPERATION
BOOL
__WaitForEngine(
	_In_ 		HANDLE 			hEngine,
	_In_ 		ULONGLONG 		ullMilliseconds
){
	EXIT_IF_UNLIKELY_NULL(hEngine, FALSE);

	LPENGINE lpEngine;
	lpEngine = (LPENGINE)GetHandleDerivative(hEngine);

	return WaitForSingleObject(lpEngine->hThread, ullMilliseconds);
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
CreateEngine(
	_In_Opt_Z_ 	LPSTR RESTRICT		lpszEngineName,
	_In_ 		LOCK_TYPE		ltGlobalLockType,
	_In_Opt_ 	ULONG 			ulLockFlag,
	_In_ 		LPFN_ENGINE_UPDATE_PROC	lpfnUpdate,
	_In_Opt_ 	LPVOID 			lpUpdateParam,
	_In_ 		BOOL 			bThreeDimensionalSupport
){
	UNREFERENCED_PARAMETER(bThreeDimensionalSupport);

	HANDLE hEngine;
	LPENGINE lpEngine;

	if (FALSE == PodNetEngineHasBeenInitialized())
	{ InitializePodNetEngine(); }

	lpEngine = GlobalAllocAndZero(sizeof(ENGINE));
	EXIT_IF_UNLIKELY_NULL(lpEngine, NULL_OBJECT);

	switch (ltGlobalLockType)
	{

		case LOCK_TYPE_CRITICAL_SECTION:
		{
			if (0 == ulLockFlag)
			{ ulLockFlag = 0x0C00; }

			lpEngine->hGlobalLock= CreateCriticalSectionAndSpecifySpinCount(ulLockFlag);
			lpEngine->hRenderLock= CreateCriticalSectionAndSpecifySpinCount(ulLockFlag);
			lpEngine->hInputLock= CreateCriticalSectionAndSpecifySpinCount(ulLockFlag);
			break;
		}

		case LOCK_TYPE_EVENT:
		{
			lpEngine->hGlobalLock = CreateEvent(FALSE);
			lpEngine->hRenderLock = CreateEvent(FALSE);
			lpEngine->hInputLock = CreateEvent(FALSE);
			break;
		}

		case LOCK_TYPE_INVALID:
		{ 
			FreeMemory(lpEngine);
			return NULL_OBJECT; 
			break; 
		}

		case LOCK_TYPE_MUTEX:
		{
			lpEngine->hGlobalLock = CreateMutex();
			lpEngine->hRenderLock = CreateMutex();
			lpEngine->hInputLock = CreateMutex();
			break;
		}

		case LOCK_TYPE_SEMAPHORE:
		{
			if (0 == ulLockFlag)
			{ ulLockFlag = 1; }

			lpEngine->hGlobalLock = CreateSemaphore(ulLockFlag);
			lpEngine->hRenderLock = CreateSemaphore(ulLockFlag);
			lpEngine->hInputLock = CreateSemaphore(ulLockFlag);
			break;			
		}

		case LOCK_TYPE_SPIN_LOCK:
		{
			lpEngine->hGlobalLock = CreateSpinLock();
			lpEngine->hRenderLock = CreateSpinLock();
			lpEngine->hInputLock = CreateSpinLock();
			break;
		}

		case LOCK_TYPE_SIZE:
		{
			FreeMemory(lpEngine);
			return NULL_OBJECT;
			break;
		}

		default: return NULL_OBJECT;

	}

	StringCopySafe(
		lpEngine->csEngineName,
		lpszEngineName,
		sizeof(lpEngine->csEngineName) - 1
	);


	lpEngine->hvRenderers = CreateVector(ENGINE_VECTOR_SIZE, sizeof(HANDLE), 0);
	lpEngine->hvRenderStartEvents = CreateVector(ENGINE_VECTOR_SIZE, sizeof(HANDLE), 0);
	lpEngine->hvWindows = CreateVector(ENGINE_VECTOR_SIZE, sizeof(HANDLE), 0);
	lpEngine->lpfnUpdate = lpfnUpdate;
	lpEngine->lpUpdateParam = lpUpdateParam;
	lpEngine->hStart = CreateEvent(FALSE);
	lpEngine->bRunning = TRUE;

	hEngine = CreateHandleWithSingleInheritor(
		lpEngine,
		&(lpEngine->hThis),
		HANDLE_TYPE_ENGINE,
		__DestroyEngine,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpEngine->ullId),
		0
	);

	SetHandleEventWaitFunction(hEngine, __WaitForEngine, 0);

	return hEngine;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
BOOL
StartEngine(
	_In_ 		HANDLE 			hEngine
){
	EXIT_IF_UNLIKELY_NULL(hEngine, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hEngine), HANDLE_TYPE_ENGINE, FALSE);

	LPENGINE lpEngine;
	lpEngine = (LPENGINE)GetHandleDerivative(hEngine);
	EXIT_IF_UNLIKELY_NULL(lpEngine, FALSE);

	SignalEvent(lpEngine->hStart);
	__EngineLoop(lpEngine);

	return TRUE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
EngineSetMainWindow(
	_In_ 		HANDLE RESTRICT		hEngine,
	_In_ 		HANDLE RESTRICT		hWindow
){
	EXIT_IF_UNLIKELY_NULL(hEngine, FALSE);
	EXIT_IF_UNLIKELY_NULL(hWindow, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hEngine), HANDLE_TYPE_ENGINE, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindow), HANDLE_TYPE_RENDERABLE_WINDOW, FALSE);

	LPENGINE lpEngine;
	lpEngine = (LPENGINE)GetHandleDerivative(hEngine);
	EXIT_IF_UNLIKELY_NULL(lpEngine, FALSE);

	WaitForSingleObject(lpEngine->hGlobalLock, INFINITE_WAIT_TIME);

	*(LPHANDLE)VectorAt(lpEngine->hvWindows, 0) = hWindow;

	ReleaseSingleObject(lpEngine->hGlobalLock);

	return TRUE;
}




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
EngineSetMainRenderer(
	_In_ 		HANDLE RESTRICT		hEngine,
	_In_ 		HANDLE RESTRICT		hRenderer
){
	EXIT_IF_UNLIKELY_NULL(hEngine, FALSE);
	EXIT_IF_UNLIKELY_NULL(hRenderer, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hEngine), HANDLE_TYPE_ENGINE, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, FALSE);

	LPENGINE lpEngine;
	lpEngine = (LPENGINE)GetHandleDerivative(hEngine);
	EXIT_IF_UNLIKELY_NULL(lpEngine, FALSE);

	WaitForSingleObject(lpEngine->hGlobalLock, INFINITE_WAIT_TIME);

	*(LPHANDLE)VectorAt(lpEngine->hvRenderers, 0) = hRenderer;

	ReleaseSingleObject(lpEngine->hGlobalLock);

	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
EngineGetGlobalLock(
	_In_ 		HANDLE 			hEngine
){
	EXIT_IF_UNLIKELY_NULL(hEngine, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hEngine), HANDLE_TYPE_ENGINE, NULL_OBJECT);

	LPENGINE lpEngine;
	lpEngine = (LPENGINE)GetHandleDerivative(hEngine);
	EXIT_IF_UNLIKELY_NULL(lpEngine, NULL_OBJECT);

	return lpEngine->hGlobalLock;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
EngineGetRenderLock(
	_In_ 		HANDLE 			hEngine
){
	EXIT_IF_UNLIKELY_NULL(hEngine, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hEngine), HANDLE_TYPE_ENGINE, NULL_OBJECT);

	LPENGINE lpEngine;
	lpEngine = (LPENGINE)GetHandleDerivative(hEngine);
	EXIT_IF_UNLIKELY_NULL(lpEngine, NULL_OBJECT);

	return lpEngine->hRenderLock;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
EngineGetInputLock(
	_In_ 		HANDLE 			hEngine
){
	EXIT_IF_UNLIKELY_NULL(hEngine, NULL_OBJECT);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hEngine), HANDLE_TYPE_ENGINE, NULL_OBJECT);

	LPENGINE lpEngine;
	lpEngine = (LPENGINE)GetHandleDerivative(hEngine);
	EXIT_IF_UNLIKELY_NULL(lpEngine, NULL_OBJECT);

	return lpEngine->hInputLock;
}




_Success_(return != NAN32, _Interlocked_Operation_)
PODNETENGINE_API
FLOAT
EngineGetLastFps(
	_In_ 		HANDLE 			hEngine
){
	EXIT_IF_UNLIKELY_NULL(hEngine, NAN32);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hEngine), HANDLE_TYPE_ENGINE, NAN32);

	LPENGINE lpEngine;
	lpEngine = (LPENGINE)GetHandleDerivative(hEngine);
	EXIT_IF_UNLIKELY_NULL(lpEngine, NAN32);

	return lpEngine->fLastFrameFps;
}




_Success_(return != FALSE, _Interlocked_Operation_)
PODNETENGINE_API
BOOL
EngineSetMaxFps(
	_In_ 		HANDLE 			hEngine,
	_In_		ULONG 			ulFps
){
	EXIT_IF_UNLIKELY_NULL(hEngine, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hEngine), HANDLE_TYPE_ENGINE, FALSE);

	LPENGINE lpEngine;
	lpEngine = (LPENGINE)GetHandleDerivative(hEngine);
	EXIT_IF_UNLIKELY_NULL(lpEngine, FALSE);

	lpEngine->ulGoalFps = ulFps;
	return TRUE;
}