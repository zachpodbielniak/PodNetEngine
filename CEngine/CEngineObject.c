#ifndef PODNETENGINE_ENGINEOBJECT_C
#define PODNETENGINE_ENGINEOBJECT_C


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"
#include "CEngine.h"



typedef struct __ENGINE
{
	INHERITS_FROM_HANDLE();
	HANDLE 				hThread;
	HANDLE 				hGlobalLock;
	HANDLE 				hRenderLock;
	HANDLE 				hStart;
	HANDLE 				hInputLock;
	LOCK_TYPE			ltLockType;

	HVECTOR				hvRenderers;		/* Handle To Renderer */
	HVECTOR 			hvRenderStartEvents;	/* Event Handle To Signal Renderer To Start */
	HVECTOR				hvWindows;		/* Support For More Than One Window */
	
	HANDLE 				hEventHandler;
	
	LPFN_ENGINE_UPDATE_PROC		lpfnUpdate;
	LPVOID 				lpUpdateParam;

	TIMESPEC			tmSpec;

	CSTRING 			csEngineName[0x100];

	FLOAT 				fLastFrameFps;
	ULONG 				ulGoalFps;
	BOOL 				bThreeDimensionalSupport;
	BOOL 				bRunning;
} ENGINE, *LPENGINE;


#endif