#ifndef PODNETENGINE_ENGINE_H
#define PODNETENGINE_ENGINE_H



#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"
#include "../CInit/CInit.h"
#include "../CWindow/CWindow.h"




typedef _Call_Back_ BOOL(* LPFN_ENGINE_UPDATE_PROC)(
	_In_ 		HANDLE 			hEngine,
	_In_Opt_ 	LPVOID 			lpParam
);






_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
CreateEngine(
	_In_Opt_Z_ 	LPSTR RESTRICT		lpszEngineName,
	_In_ 		LOCK_TYPE		ltGlobalLockType,
	_In_Opt_ 	ULONG 			ulLockFlag,
	_In_		LPFN_ENGINE_UPDATE_PROC	lpfnUpdate,
	_In_Opt_ 	LPVOID 			lpUpdateParam,
	_In_ 		BOOL 			bThreeDimensionalSupport
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
BOOL
StartEngine(
	_In_ 		HANDLE 			hEngine
);




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
EngineSetMainWindow(
	_In_ 		HANDLE RESTRICT		hEngine,
	_In_ 		HANDLE RESTRICT		hWindow
);




_Success_(return != FALSE, _Acquires_Shared_Lock_())
PODNETENGINE_API
BOOL
EngineSetMainRenderer(
	_In_ 		HANDLE RESTRICT		hEngine,
	_In_ 		HANDLE RESTRICT		hWindow
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
EngineGetGlobalLock(
	_In_ 		HANDLE 			hEngine
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
EngineGetRenderLock(
	_In_ 		HANDLE 			hEngine
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
EngineGetInputLock(
	_In_ 		HANDLE 			hEngine
);




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
EngineGetRenderStartEvent(
	_In_ 		HANDLE 			hEngine
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNETENGINE_API
BOOL
EngineSetRenderer(
	_In_ 		HANDLE 			hRenderer
);




_Success_(return != MAX_ULONG, _Acquires_Shared_Lock_())
PODNETENGINE_API
ULONG
EngineAddRenderableWindow(
	_In_ 		HANDLE 			hWindow
);




_Success_(return != NAN32, _Interlocked_Operation_)
PODNETENGINE_API
FLOAT
EngineGetLastFps(
	_In_ 		HANDLE 			hEngine
);




_Success_(return != FALSE, _Interlocked_Operation_)
PODNETENGINE_API
BOOL
EngineSetMaxFps(
	_In_ 		HANDLE 			hEngine,
	_In_		ULONG 			ulFps
);









#endif