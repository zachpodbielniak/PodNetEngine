#include "CVertex.h"




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
LPVERTEX
CreateVertex(
	_In_ 		FLOAT 		fX,
	_In_ 		FLOAT 		fY,
	_In_ 		FLOAT 		fZ
){
	LPVERTEX lpVertex;
	lpVertex = GlobalAllocAndZero(sizeof(VERTEX));
	EXIT_IF_UNLIKELY_NULL(lpVertex, NULLPTR);

	lpVertex->vec3Position.fX = fX;
	lpVertex->vec3Position.fY = fY;
	lpVertex->vec3Position.fZ = fZ;

	return lpVertex;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
LPVERTEX
CreateVertexFromVector3D(
	_In_ 		LPVECTOR3D 	lpvec3Data
){
	EXIT_IF_UNLIKELY_NULL(lpvec3Data, NULLPTR);

	LPVERTEX lpVertex;
	lpVertex = GlobalAllocAndZero(sizeof(VERTEX));
	EXIT_IF_UNLIKELY_NULL(lpVertex, NULLPTR);

	CopyMemory(&(lpVertex->vec3Position), lpvec3Data, sizeof(VECTOR3D));
	return lpVertex;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
BOOL
InitializeVertex(
	_In_ 		LPVERTEX	lpvtxData,
	_In_ 		FLOAT 		fX,
	_In_ 		FLOAT 		fY,
	_In_ 		FLOAT 		fZ
){
	EXIT_IF_UNLIKELY_NULL(lpvtxData, FALSE);

	lpvtxData->vec3Position.fX = fX;
	lpvtxData->vec3Position.fY = fY;
	lpvtxData->vec3Position.fZ = fZ;

	return TRUE;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
BOOL
InitializeVertexFromVector3D(
	_In_		LPVERTEX	lpvtxData,
	_In_ 		LPVECTOR3D	lpvec3Data
){
	EXIT_IF_UNLIKELY_NULL(lpvtxData, FALSE);
	EXIT_IF_UNLIKELY_NULL(lpvec3Data, FALSE);

	CopyMemory(&(lpvtxData->vec3Position), lpvec3Data, sizeof(VECTOR3D));
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
DestroyVertex(
	_In_ 		LPVERTEX 	lpvtxVertex
){
	EXIT_IF_LIKELY_NULL(lpvtxVertex, FALSE);
	FreeMemory(lpvtxVertex);
	return TRUE;
}