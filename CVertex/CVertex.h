#ifndef PODNETENGINE_VERTEX_H
#define PODNETENGINE_VERTEX_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"




typedef struct __VERTEX 
{
	VECTOR3D	vec3Position;
} VERTEX, *LPVERTEX;




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
LPVERTEX
CreateVertex(
	_In_ 		FLOAT 		fX,
	_In_ 		FLOAT 		fY,
	_In_ 		FLOAT 		fZ
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
LPVERTEX
CreateVertexFromVector3D(
	_In_ 		LPVECTOR3D	lpvec3Data
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
BOOL
InitializeVertex(
	_In_ 		LPVERTEX	lpvtxData,
	_In_ 		FLOAT 		fX,
	_In_ 		FLOAT 		fY,
	_In_ 		FLOAT 		fZ
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
BOOL
InitializeVertexFromVector3D(
	_In_		LPVERTEX	lpvtxData,
	_In_ 		LPVECTOR3D	lpvec3Data
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
DestroyVertex(
	_In_ 		LPVERTEX 	lpvtxVertex
);



#endif