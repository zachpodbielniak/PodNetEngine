#include "../PodNetEngine.h"
#include <PodNet/CMath/CVector2D.h>

#define TITLE 		"Pong -- PodNetEngine Edition"
#define WND_POS_X 	100
#define WND_POS_Y 	100
#define WND_WIDTH	1280
#define WND_HEIGHT	720


typedef struct __PONG
{
	HANDLE 		hEngine;
	HANDLE 		hEventHandler;
	HANDLE 		hRenderer;
	HANDLE 		hWindow;

	HDRAWABLE	hdBall;
	HDRAWABLE	hdPaddleLeft;
	HDRAWABLE	hdPaddleRight;
	HDRAWABLE	hdLeftScore;
	HDRAWABLE	hdRightScore;
	HDRAWABLE	hdFps;


	VECTOR2D	v2BallVelocity;
	ULONG 		ulLeftScore;
	ULONG 		ulRightScore;

	COLOR 		clrWhite;
	COLOR 		clrBall;
	COLOR 		clrPaddleLeft;
	COLOR 		clrPaddleRight;
} PONG, *LPPONG;




_Call_Back_
_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__KeyPressedEsc(
	_In_ 		HANDLE 		hEngine,
	_In_ 		HANDLE 		hEventHandler,
	_In_Opt_ 	LPVOID 		lpParam,
	_In_		KEY 		kKeyPressed	
){ 
	UNREFERENCED_PARAMETER(hEngine);
	UNREFERENCED_PARAMETER(hEventHandler);
	UNREFERENCED_PARAMETER(lpParam);
	UNREFERENCED_PARAMETER(kKeyPressed);

	PostQuitMessage(0);
	return TRUE;
}




_Call_Back_
_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__KeyPressedMovement(
	_In_ 		HANDLE 		hEngine,
	_In_ 		HANDLE 		hEventHandler,
	_In_Opt_ 	LPVOID 		lpParam,
	_In_		KEY 		kKeyPressed	
){
	UNREFERENCED_PARAMETER(hEngine);
	UNREFERENCED_PARAMETER(hEventHandler);

	LPPONG lpGame;
	ULONG ulY;
	lpGame = (LPPONG)lpParam;

	switch ((ULONG)kKeyPressed)
	{

		case (ULONG)KEY_W:
		{
			SetDrawablePositionByOffset(lpGame->hdPaddleLeft, 0, -5);
			break;
		}

		case (ULONG)KEY_S:
		{
			SetDrawablePositionByOffset(lpGame->hdPaddleLeft, 0, 5);
			break;
		}

		case (ULONG)KEY_UP:
		{
			SetDrawablePositionByOffset(lpGame->hdPaddleRight, 0, -5);
			break;
		}

		case (ULONG)KEY_DOWN:
		{
			SetDrawablePositionByOffset(lpGame->hdPaddleRight, 0, 5);
			break;
		}

		default: return FALSE;
	}

	GetDrawablePositionY(lpGame->hdPaddleLeft, ulY);	

	if ((LONG)ulY >= (WND_HEIGHT - 70))
	{ SetDrawablePositionY(lpGame->hdPaddleLeft, WND_HEIGHT - 70); }
	else if ((LONG)ulY <= 10)
	{ SetDrawablePositionY(lpGame->hdPaddleLeft, 10); }

	GetDrawablePositionY(lpGame->hdPaddleRight, ulY);

	if ((LONG)ulY >= (WND_HEIGHT - 70))
	{ SetDrawablePositionY(lpGame->hdPaddleRight, WND_HEIGHT - 70); }
	else if ((LONG)ulY <= 10)
	{ SetDrawablePositionY(lpGame->hdPaddleRight, 10); }


	return TRUE;
}




_Call_Back_
_Success_(return != FALSE, _Interlocked_Operation_)
INTERNAL_OPERATION
BOOL
__Update(
	_In_ 		HANDLE			hEngine,
	_In_Opt_ 	LPVOID 			lpParam
){
	UNREFERENCED_PARAMETER(hEngine);

	LPPONG lpGame;
	ULONG ulX, ulY;
	ULONG ulBallX, ulBallY;
	CSTRING csBuffer[0x20];

	lpGame = (LPPONG)lpParam;

	ZeroMemory(csBuffer, sizeof(csBuffer));
	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"%u", 
		lpGame->ulLeftScore
	);

	lpGame->hdLeftScore = CreateDrawableText(
		lpGame->hRenderer,
		csBuffer,
		"/usr/share/fonts/TTF/arial.ttf",
		24,
		lpGame->clrWhite
	);
	
	ZeroMemory(csBuffer, sizeof(csBuffer));
	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"%u", 
		lpGame->ulRightScore
	);

	lpGame->hdRightScore = CreateDrawableText(
		lpGame->hRenderer,
		csBuffer,
		"/usr/share/fonts/TTF/arial.ttf",
		24,
		lpGame->clrWhite
	);

	SetDrawablePosition(
		lpGame->hdLeftScore,
		100,
		10
	);

	GetDrawableSizeWidth(lpGame->hdPaddleRight, ulX);

	SetDrawablePosition(
		lpGame->hdRightScore,
		WND_WIDTH - ulX - 100,
		10
	);

	
	/* FPS */
	ZeroMemory(csBuffer, sizeof(csBuffer));
	StringPrintFormatSafe(
		csBuffer,
		sizeof(csBuffer) - 1,
		"FPS: %.1f",
		EngineGetLastFps(lpGame->hEngine)
	);

	lpGame->hdFps = CreateDrawableText(
		lpGame->hRenderer,
		csBuffer,
		"/usr/share/fonts/TTF/arial.ttf",
		16,
		lpGame->clrWhite
	);

	GetDrawableSizeWidth(lpGame->hdFps, ulX);
	SetDrawablePosition(
		lpGame->hdFps,
		(WND_WIDTH / 2) - (ulX / 2),
		WND_HEIGHT - 30
	);



	/* Ball Update */
	SetDrawablePositionByOffset(
		lpGame->hdBall,
		(ULONG)lpGame->v2BallVelocity.fX,
		(ULONG)lpGame->v2BallVelocity.fY
	);

	/* Check For Paddle */
	GetDrawablePosition(
		lpGame->hdBall,
		&ulBallX,
		&ulBallY
	);

	GetDrawablePosition(
		lpGame->hdPaddleLeft,
		&ulX,
		&ulY
	);

	if (ulBallX <= ulX + 10)
	{ 
		if (
			(ulY - 10 <= ulBallY) &&
			(ulY + 60 >= ulBallY )
		){ 
			lpGame->v2BallVelocity.fX *= -1.0f;
			JUMP(__BallCollisionChecked);
		}
		else
		{
			lpGame->ulRightScore++;
			SetDrawablePosition(
				lpGame->hdBall,
				WND_WIDTH / 2,
				WND_HEIGHT / 2
			);

			lpGame->v2BallVelocity.fX *= -1.0f;
			JUMP(__BallCollisionChecked);
		}
	}

	GetDrawablePosition(
		lpGame->hdPaddleRight,
		&ulX,
		&ulY
	);

	if (ulBallX >= ulX)
	{
		if (
			(ulY - 10 <= ulBallY) &&
			(ulY + 60 >= ulBallY)
		){
			lpGame->v2BallVelocity.fX *= -1.0f;
			JUMP(__BallCollisionChecked);
		}
		else
		{
			lpGame->ulLeftScore++;
			SetDrawablePosition(
				lpGame->hdBall,
				WND_WIDTH / 2,
				WND_HEIGHT / 2
			);

			lpGame->v2BallVelocity.fX *= -1.0f;
			JUMP(__BallCollisionChecked);
		}
	}

	__BallCollisionChecked:


	if (ulBallY >= (WND_HEIGHT - 20))
	{ lpGame->v2BallVelocity.fY *= -1.0f; }
	else if (ulBallY <= 20)
	{ lpGame->v2BallVelocity.fY *= -1.0f; }


	SetRenderDrawColor(lpGame->hRenderer, 0, 0, 255, 255);
	ClearRenderBuffer(lpGame->hRenderer);

	HandleInput(lpGame->hEventHandler);

	Draw(lpGame->hdBall, NULLPTR);
	Draw(lpGame->hdPaddleLeft, NULLPTR);
	Draw(lpGame->hdPaddleRight, NULLPTR);
	Draw(lpGame->hdLeftScore, NULLPTR);
	Draw(lpGame->hdRightScore, NULLPTR);
	Draw(lpGame->hdFps, NULLPTR);

	Render(lpGame->hRenderer);


	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__InitPong(
	_In_ 		LPPONG			lpGame
){
	EXIT_IF_UNLIKELY_NULL(lpGame, FALSE);

	lpGame->hEngine = CreateEngine(
		TITLE,
		LOCK_TYPE_MUTEX,
		0,
		__Update,
		lpGame,
		FALSE
	);


	lpGame->hWindow = CreateRenderableWindow(
		TITLE,
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		WND_HEIGHT,
		WND_WIDTH,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL
	);

	lpGame->hRenderer = CreateRenderer(
		lpGame->hWindow,
		SDL_RENDERER_ACCELERATED
	);

	lpGame->hEventHandler = CreateEventHandler(lpGame->hEngine);

	EngineSetMainRenderer(lpGame->hEngine, lpGame->hRenderer);
	EngineSetMainWindow(lpGame->hEngine, lpGame->hWindow);
	EngineSetMaxFps(lpGame->hEngine, 75);
	
	lpGame->clrWhite.ulValue = 0xFFFFFFFF;
	lpGame->ulLeftScore = 0;
	lpGame->ulRightScore = 0;

	lpGame->v2BallVelocity.fX = 10.0f;
	lpGame->v2BallVelocity.fY = -3.0f;

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__CreateDrawables(
	_In_ 		LPPONG			lpGame
){
	EXIT_IF_UNLIKELY_NULL(lpGame, FALSE);

	lpGame->hdPaddleLeft = CreateDrawableShape(
		lpGame->hRenderer,
		SHAPE_RECTANGLE,
		TRUE
	);

	lpGame->hdPaddleRight = CreateDrawableShape(
		lpGame->hRenderer,
		SHAPE_RECTANGLE,
		TRUE
	);

	lpGame->hdBall = CreateDrawableShape(
		lpGame->hRenderer,
		SHAPE_CIRCLE,
		TRUE
	);

	SetDrawablePositionAndSize(
		lpGame->hdPaddleLeft,
		10,
		WND_HEIGHT / 2,
		10,
		60
	);

	SetDrawablePositionAndSize(
		lpGame->hdPaddleRight,
		WND_WIDTH - 20,
		WND_HEIGHT / 2,
		10,
		60
	);

	SetDrawablePositionAndSize(
		lpGame->hdBall,
		WND_WIDTH / 2,
		WND_HEIGHT / 2,
		10,
		0
	);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__RegisterCallBacks(
	_In_ 		LPPONG			lpGame
){
	EXIT_IF_UNLIKELY_NULL(lpGame, FALSE);

	RegisterEventKeyHandlerCallBack(
		lpGame->hEventHandler,
		KEY_ESCAPE,
		__KeyPressedEsc,
		NULLPTR,
		TRUE
	);

	RegisterEventKeyHandlerCallBack(
		lpGame->hEventHandler,
		KEY_W,
		__KeyPressedMovement,
		lpGame,
		TRUE
	);

	RegisterEventKeyHandlerCallBack(
		lpGame->hEventHandler,
		KEY_S,
		__KeyPressedMovement,
		lpGame,
		TRUE
	);

	RegisterEventKeyHandlerCallBack(
		lpGame->hEventHandler,
		KEY_UP,
		__KeyPressedMovement,
		lpGame,
		TRUE
	);
	
	RegisterEventKeyHandlerCallBack(
		lpGame->hEventHandler,
		KEY_DOWN,
		__KeyPressedMovement,
		lpGame,
		TRUE
	);

	return TRUE;
}




LONG Main(
	_In_ 		LONG 			lArgCount,
	_In_		DLPSTR 			dlpszArgValues
){
	UNREFERENCED_PARAMETER(lArgCount);
	UNREFERENCED_PARAMETER(dlpszArgValues);

	PONG pGame;

	__InitPong(&pGame);
	__CreateDrawables(&pGame);
	__RegisterCallBacks(&pGame);
	StartEngine(pGame.hEngine);


	return 0;
}