#include "CTexture.h"
#include "../CRenderer/CRendererObject.c"



_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
LPTEXTURE
CreateTexture(
	_In_ 		HANDLE 			hRenderer,
	_In_ 		LPSTR RESTRICT		lpszFile,
	_In_		TEXTURE_FILE_TYPE	tftFileType
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpszFile, NULLPTR);

	LPTEXTURE lptxTex;
	LPRENDERER lpRenderer;
	
	lpRenderer = (LPRENDERER)GetHandleDerivative(hRenderer);
	EXIT_IF_UNLIKELY_NULL(lpRenderer, NULLPTR);
	
	lptxTex = GlobalAllocAndZero(sizeof(TEXTURE));
	EXIT_IF_UNLIKELY_NULL(lptxTex, NULLPTR);


	lptxTex->lpsdlTexture = IMG_LoadTexture(
		lpRenderer->lpsdlRenderer,
		lpszFile		
	);

	lptxTex->tftFileType = tftFileType;
	
	return lptxTex;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
LPTEXTURE
CreateTextureFromText(
	_In_ 		HANDLE			hRenderer,
	_In_		LPSTR RESTRICT		lpszText,
	_In_ 		LPSTR RESTRICT 		lpszFontFile,
	_In_		ULONG 			ulFontSize,
	_In_ 		COLOR 			clrTextColor
){
	EXIT_IF_UNLIKELY_NULL(hRenderer, NULLPTR);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hRenderer), HANDLE_TYPE_RENDERER, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpszText, NULLPTR);
	EXIT_IF_UNLIKELY_NULL(lpszFontFile, NULLPTR);

	LPTEXTURE lptxTex;
	LPRENDERER lpRenderer;
	TTF_Font *ttfFont;
	SDL_Surface *surf;

	lpRenderer = (LPRENDERER)GetHandleDerivative(hRenderer);
	EXIT_IF_UNLIKELY_NULL(lpRenderer, NULLPTR);
	
	ttfFont = TTF_OpenFont(lpszFontFile, (LONG)ulFontSize);
	EXIT_IF_UNLIKELY_NULL(ttfFont, NULLPTR);	

	surf = TTF_RenderText_Blended(ttfFont, lpszText, clrTextColor.sdlColor);
	if (NULLPTR == surf)
	{
		TTF_CloseFont(ttfFont);
		return NULLPTR;
	}

	lptxTex = GlobalAllocAndZero(sizeof(TEXTURE));
	EXIT_IF_UNLIKELY_NULL(lptxTex, NULLPTR);

	if (0 == ulFontSize)
	{ ulFontSize = 12; }

	/* TODO: Add Font Type */
	lptxTex->tftFileType = TEXTURE_FILE_UNKNOWN;
	lptxTex->lpsdlTexture = SDL_CreateTextureFromSurface(lpRenderer->lpsdlRenderer, surf);

	SDL_FreeSurface(surf);
	TTF_CloseFont(ttfFont);
	return lptxTex;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
DestroyTexture(
	_In_ 		LPTEXTURE		lptxTexture
){
	EXIT_IF_UNLIKELY_NULL(lptxTexture, FALSE);

	SDL_DestroyTexture(lptxTexture->lpsdlTexture);
	FreeMemory(lptxTexture);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
QueryTexture(
	_In_ 		LPTEXTURE 		lptxTexture
){
	EXIT_IF_UNLIKELY_NULL(lptxTexture, FALSE);

	SDL_QueryTexture(
		lptxTexture->lpsdlTexture,
		NULLPTR,
		NULLPTR,
		(LPLONG)&(lptxTexture->wrectCoordinates.ulW),
		(LPLONG)&(lptxTexture->wrectCoordinates.ulH)
	);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
UpdateTexturePosition(
	_In_		LPTEXTURE 		lptxTexture,
	_In_ 		ULONG 			ulPositionX,
	_In_		ULONG 			ulPositionY
){
	EXIT_IF_UNLIKELY_NULL(lptxTexture, FALSE);

	lptxTexture->wrectCoordinates.ulX = ulPositionX;
	lptxTexture->wrectCoordinates.ulY = ulPositionY;
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
UpdateTexturePositionByOffset(
	_In_ 		LPTEXTURE 		lptxTexture,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY
){
	EXIT_IF_UNLIKELY_NULL(lptxTexture, FALSE);

	lptxTexture->wrectCoordinates.ulX += ulPositionX;
	lptxTexture->wrectCoordinates.ulY += ulPositionY;
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
UpdateTextureSize(
	_In_ 		LPTEXTURE 		lptxTexture,
	_In_		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight
){
	EXIT_IF_UNLIKELY_NULL(lptxTexture, FALSE);

	lptxTexture->wrectCoordinates.ulW = ulWidth;
	lptxTexture->wrectCoordinates.ulH = ulHeight;
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
UpdateTextureSizeByOffset(
	_In_ 		LPTEXTURE 		lptxTexture,
	_In_		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight
){
	EXIT_IF_UNLIKELY_NULL(lptxTexture, FALSE);

	lptxTexture->wrectCoordinates.ulW += ulWidth;
	lptxTexture->wrectCoordinates.ulH += ulHeight;
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
UpdateTexturePositionAndSize(
	_In_ 		LPTEXTURE 		lptxTexture,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_		ULONG 			ulWidth,
	_In_		ULONG 			ulHeight
){
	EXIT_IF_UNLIKELY_NULL(lptxTexture, FALSE);

	lptxTexture->wrectCoordinates.ulX = ulPositionX;
	lptxTexture->wrectCoordinates.ulY = ulPositionY;
	lptxTexture->wrectCoordinates.ulW = ulWidth;
	lptxTexture->wrectCoordinates.ulH = ulHeight;
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
UpdateTexturePositionAndSizeByOffset(
	_In_ 		LPTEXTURE 		lptxTexture,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_		ULONG 			ulWidth,
	_In_		ULONG 			ulHeight
){
	EXIT_IF_UNLIKELY_NULL(lptxTexture, FALSE);

	lptxTexture->wrectCoordinates.ulX += ulPositionX;
	lptxTexture->wrectCoordinates.ulY += ulPositionY;
	lptxTexture->wrectCoordinates.ulW += ulWidth;
	lptxTexture->wrectCoordinates.ulH += ulHeight;
	return TRUE;
}