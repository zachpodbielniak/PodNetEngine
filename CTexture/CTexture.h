#ifndef PODNETENGINE_TEXTURE_H
#define PODNETENGINE_TEXTURE_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"

typedef enum __TEXTURE_FILE_TYPE
{
	TEXTURE_FILE_UNKNOWN 		= 0,
	TEXTURE_FILE_BMP		= 1,
	TEXTURE_FILE_GIF		= 2,
	TEXTURE_FILE_JPEG		= 3,
	TEXTURE_FILE_LBM		= 4,
	TEXTURE_FILE_PCX		= 5,
	TEXTURE_FILE_PNG		= 6,
	TEXTURE_FILE_PNM		= 7,
	TEXTURE_FILE_SVG		= 8,

	TEXTURE_FILE_TGA		= 9,
	TEXTURE_FILE_TIFF		= 10,
	TEXTURE_FILE_WEBP		= 11,
	TEXTURE_FILE_XCF		= 12,
	TEXTURE_FILE_XPM		= 13,
	TEXTURE_FILE_XV			= 14
} TEXTURE_FILE_TYPE;




typedef struct __TEXTURE 
{
	LPSDLTEXTURE		lpsdlTexture;
	WINDOWRECT		wrectCoordinates;
	TEXTURE_FILE_TYPE	tftFileType;
} TEXTURE, *LPTEXTURE, **DLPTEXTURE;




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
LPTEXTURE
CreateTexture(
	_In_ 		HANDLE 			hRenderer,
	_In_ 		LPSTR RESTRICT		lpszFile,
	_In_		TEXTURE_FILE_TYPE	tftFileType
);




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
LPTEXTURE
CreateTextureFromText(
	_In_ 		HANDLE			hRenderer,
	_In_		LPSTR RESTRICT		lpszText,
	_In_ 		LPSTR RESTRICT 		lpszFontFile,
	_In_		ULONG 			ulFontSize,
	_In_ 		COLOR 			clrTextColor
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
DestroyTexture(
	_In_ 		LPTEXTURE		lptxTexture
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
QueryTexture(
	_In_ 		LPTEXTURE 		lptxTexture
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
UpdateTexturePosition(
	_In_		LPTEXTURE 		lptxTexture,
	_In_ 		ULONG 			ulPositionX,
	_In_		ULONG 			ulPositionY
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
UpdateTexturePositionByOffset(
	_In_ 		LPTEXTURE 		lptxTexture,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
UpdateTextureSize(
	_In_ 		LPTEXTURE 		lptxTexture,
	_In_		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
UpdateTextureSizeByOffset(
	_In_ 		LPTEXTURE 		lptxTexture,
	_In_		ULONG 			ulWidth,
	_In_ 		ULONG 			ulHeight
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
UpdateTexturePositionAndSize(
	_In_ 		LPTEXTURE 		lptxTexture,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_		ULONG 			ulWidth,
	_In_		ULONG 			ulHeight
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
UpdateTexturePositionAndSizeByOffset(
	_In_ 		LPTEXTURE 		lptxTexture,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_		ULONG 			ulWidth,
	_In_		ULONG 			ulHeight
);



#endif