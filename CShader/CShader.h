#ifndef PODNETENGINE_SHADER_H
#define PODNETENGINE_SHADER_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"


#define NUM_SHADERS		0x02




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
HANDLE
CreateShader(
	_In_ 		LPCSTR RESTRICT		lpcszFileName
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
ShaderBind(
	_In_		HANDLE 			hShader
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
ShaderUpdate(
	_In_		HANDLE 			hShader
);




#endif