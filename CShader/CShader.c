#include "CShader.h"
#include "CShaderObject.c"




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__DestroyShader(
	_In_ 		HDERIVATIVE 		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);
	EXIT_IF_UNLIKELY_NULL(hDerivative, FALSE);

	LPSHADER lpShader;
	lpShader = (LPSHADER)hDerivative;

	glDetachShader(lpShader->ulProgramId, lpShader->lpulShaders[0]);
	glDetachShader(lpShader->ulProgramId, lpShader->lpulShaders[1]);

	glDeleteShader(lpShader->lpulShaders[0]);
	glDeleteShader(lpShader->lpulShaders[1]);

	glDeleteProgram(lpShader->ulProgramId);

	FreeMemory(lpShader->lpszFragmentShader);
	FreeMemory(lpShader->lpszVertexShader);
	FreeMemory(lpShader);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__LoadShaderFile(
	_In_ 		LPCSTR RESTRICT		lpcszFileName,
	_Out_ 		DLPSTR 			dlpszOut
){
	EXIT_IF_UNLIKELY_NULL(lpcszFileName, FALSE);
	EXIT_IF_UNLIKELY_NULL(dlpszOut, FALSE);

	HANDLE hFile;
	BOOL bRet;

	hFile = OpenFile(lpcszFileName, FILE_PERMISSION_READ, 0);
	EXIT_IF_UNLIKELY_NULL(hFile, FALSE);


	ReadWholeFile(hFile, dlpszOut, FILE_READ_ALLOC_OUT);
	bRet = DestroyObject(hFile);

	if (FALSE == bRet)
	{ WriteFile(GetStandardError(), "Failed to load shader file!\n", 0); }

	return bRet;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__CheckShaderError(
	_In_ 		ULONG 			ulProgramId,
	_In_ 		ULONG 			ulFlag,
	_In_ 		BOOL			bIsProgram,
	_In_ 		LPCSTR 			lpcszErrorMessage
){
	LONG lSuccess;
	CSTRING csError[1024];

	lSuccess = 0;
	ZeroMemory(csError, sizeof(csError));

	if (TRUE == bIsProgram)
	{ glGetProgramiv(ulProgramId, ulFlag, &lSuccess); }
	else
	{ glGetShaderiv(ulProgramId, ulFlag, &lSuccess); }

	if (FALSE == (BOOL)lSuccess)
	{
		if (TRUE == bIsProgram)
		{ glGetProgramInfoLog(ulProgramId, sizeof(csError), NULLPTR, csError); }
		else 
		{ glGetShaderInfoLog(ulProgramId, sizeof(csError), NULLPTR, csError); }

		WriteFile(GetStandardError(), lpcszErrorMessage, 0);
		WriteFile(GetStandardError(), csError, 0);
		WriteFile(GetStandardError(), "\n\n", 0);
	}

	return (BOOL)lSuccess;
}




_Success_(return != FALSE, _Non_Locking_)
INTERNAL_OPERATION
BOOL
__PrintOpenGlLog(
	_In_ 		LPSHADER 		lpShader
){
	EXIT_IF_UNLIKELY_NULL(lpShader, FALSE);
	
	LONG lMax, lLength;
	LPSTR lpszLog;
	
	glGetProgramiv(lpShader->ulProgramId, GL_INFO_LOG_LENGTH, &lMax);
	lpszLog = GlobalAllocAndZero(lMax + 1);
	
	glGetProgramInfoLog(lpShader->ulProgramId, lMax, &lLength, lpszLog);
	WriteFile(GetStandardError(), lpszLog, 0);

	FreeMemory(lpszLog);
	return TRUE;
}




_Success_(return != 0, _Non_Locking_)
INTERNAL_OPERATION
ULONG
__CreateShaderHelper(
	_In_ 		LPCSTR RESTRICT 	lpcszText,
	_In_ 		ULONG 			ulShaderType
){
	ULONG ulShader;

	ulShader = glCreateShader(ulShaderType);
	EXIT_IF_NULL(ulShader, 0);

	LPSTR dlpszShaderSources[1];
	ULONG ulShaderSourceLength[1];

	dlpszShaderSources[0] = (LPSTR)lpcszText;
	ulShaderSourceLength[0] = StringLength(lpcszText);

	glShaderSource(ulShader, 1, (const GLchar *const *)dlpszShaderSources, (const GLint *)ulShaderSourceLength);
	glCompileShader(ulShader);

	if (FALSE == __CheckShaderError(ulShader, GL_COMPILE_STATUS, FALSE, "Error: Shader compilation failed - "))
	{ return 0; }

	return ulShader;
}




_Success_(return != NULLPTR, _Non_Locking_)
PODNETENGINE_API
HANDLE
CreateShader(
	_In_ 		LPCSTR RESTRICT		lpcszFileName
){
	EXIT_IF_UNLIKELY_NULL(lpcszFileName, NULL_OBJECT);

	HANDLE hShader;
	LPSHADER lpShader;
	UARCHLONG ualLength;
	LPSTR lpszFragment, lpszVertex;

	lpShader = GlobalAllocAndZero(sizeof(SHADER));
	EXIT_IF_UNLIKELY_NULL(lpShader, NULL_OBJECT);

	ualLength = StringLength(lpcszFileName);
	lpShader->lpszFragmentShader = GlobalAllocAndZero((sizeof(CHAR) * ualLength) + (10));	
	lpShader->lpszVertexShader = GlobalAllocAndZero((sizeof(CHAR) * ualLength) + (10));	
	
	StringPrintFormatSafe(
		lpShader->lpszFragmentShader,
		ualLength + 9,
		"%s.fs.glsl",
		lpcszFileName
	);

	StringPrintFormatSafe(
		lpShader->lpszVertexShader,
		ualLength + 9,
		"%s.vs.glsl",
		lpcszFileName
	);

	lpShader->ulProgramId = glCreateProgram();
	
	__LoadShaderFile(lpShader->lpszVertexShader, &lpszVertex);
	__LoadShaderFile(lpShader->lpszFragmentShader, &lpszFragment);

	lpShader->lpulShaders[SHADER_TYPE_VERTEX] = __CreateShaderHelper(lpszVertex, GL_VERTEX_SHADER);
	lpShader->lpulShaders[SHADER_TYPE_FRAGMENT] = __CreateShaderHelper(lpszFragment, GL_FRAGMENT_SHADER);


	glAttachShader(lpShader->ulProgramId, lpShader->lpulShaders[0]);
	glAttachShader(lpShader->ulProgramId, lpShader->lpulShaders[1]);
	
	/* Vertex Shader Stuff, Required */
	glBindAttribLocation(lpShader->ulProgramId, 0, "position");

	glLinkProgram(lpShader->ulProgramId);
	__CheckShaderError(lpShader->ulProgramId, GL_LINK_STATUS, TRUE, "Error: Program link failed! ");


	glValidateProgram(lpShader->ulProgramId);
	__CheckShaderError(lpShader->ulProgramId, GL_VALIDATE_STATUS, TRUE, "Error: Program is invalid! ");


	hShader = CreateHandleWithSingleInheritor(
		lpShader,
		&(lpShader->hThis),
		HANDLE_TYPE_SHADER,
		__DestroyShader,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lpShader->ullId),
		0
	);
	
	FreeMemory(lpszVertex);
	FreeMemory(lpszFragment);

	return hShader;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
ShaderBind(
	_In_		HANDLE 			hShader
){
	EXIT_IF_UNLIKELY_NULL(hShader, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hShader), HANDLE_TYPE_SHADER, FALSE);

	LPSHADER lpShader;
	lpShader = (LPSHADER)GetHandleDerivative(hShader);
	EXIT_IF_UNLIKELY_NULL(lpShader, FALSE);

	glUseProgram(lpShader->ulProgramId);
	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
ShaderUpdate(
	_In_		HANDLE 			hShader
){
	EXIT_IF_UNLIKELY_NULL(hShader, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hShader), HANDLE_TYPE_SHADER, FALSE);

	LPSHADER lpShader;
	lpShader = (LPSHADER)GetHandleDerivative(hShader);
	EXIT_IF_UNLIKELY_NULL(lpShader, FALSE);

	return TRUE;
}