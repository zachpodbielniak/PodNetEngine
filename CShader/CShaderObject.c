#ifndef PODNETENGINE_SHADEROBJECT_C
#define PODNETENGINE_SHADEROBJECT_C


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"
#include "CShader.h"




typedef enum __SHADER_TYPE
{
	SHADER_TYPE_VERTEX	= 0,
	SHADER_TYPE_FRAGMENT	= 1,
	SHADER_TYPE_GEOMETRY	= 2
} SHADER_TYPE;



typedef struct __SHADER
{
	INHERITS_FROM_HANDLE();
	ULONG 		ulProgramId;
	ULONG 		lpulShaders[NUM_SHADERS];
	LPSTR 		lpszVertexShader;
	LPSTR 		lpszFragmentShader;
} SHADER, *LPSHADER;





#endif