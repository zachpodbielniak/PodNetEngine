#ifndef PODNETENGINE_TYPEDEFS_H
#define PODNETENGINE_TYPEDEFS_H


#include "Prereqs.h"
#include "Defs.h"


typedef SDL_Event		INPUTEVENT, *LPINPUTEVENT, **DLPINPUTEVENT;
typedef SDL_Window		SDLWINDOW, *LPSDLWINDOW, **DLPSDLWINDOW;
typedef SDL_Texture 		SDLTEXTURE, *LPSDLTEXTURE, **DLPSDLTEXTURE;
typedef SDL_Renderer		SDLRENDERER, *LPSDLRENDERER, **DLPSDLRENDERER;
typedef SDL_Rect		SDLRECT, *LPSDLRECT, **DLPSDLRECT;
typedef SDL_Color		SDLCOLOR, *LPSDLCOLOR, **DLPSDLCOLOR;
typedef SDL_GLContext		SDLGLCONTEXT, *LPSDLGLCONTEXT, **DLPSDLGLCONTEXT;



typedef struct __WINDOWRECT
{
	union
	{
		SDLRECT sdlRect;
		
		struct
		{
			ULONG ulX;
			ULONG ulY;
			ULONG ulW;
			ULONG ulH;
		};
	};
} WINDOWRECT, *LPWINDOWRECT, **DLPWINDOWRECT;


typedef struct __COLOR
{
	union
	{
		SDLCOLOR 	sdlColor;
		ULONG 		ulValue;
		struct
		{
			BYTE byRed;
			BYTE byGreen;
			BYTE byBlue;
			BYTE byAlpha;
		};	
	};
} COLOR, *LPCOLOR, **DLPCOLOR;



#endif