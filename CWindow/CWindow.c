#include "CWindow.h"
#include "CWindowObject.c"


_Success_(return != FALSE, _Acquires_Shared_Lock_())
INTERNAL_OPERATION
BOOL
__DestroyRenderableWindow(
	_In_ 		HDERIVATIVE 		hDerivative,
	_In_Opt_ 	ULONG 			ulFlags
){
	UNREFERENCED_PARAMETER(ulFlags);

	LPRWINDOW lprWindow;
	lprWindow = (LPRWINDOW)hDerivative;

	SDL_DestroyWindow(lprWindow->lpsdlWindow);
	FreeMemory(lprWindow);

	return TRUE;
}




_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
CreateRenderableWindow(
	_In_Z_		LPSTR RESTRICT		lpszName,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_ 		ULONG 			ulHeight,
	_In_ 		ULONG 			ulWidth,
	_In_Opt_ 	ULONG 			ulFlags
){
	EXIT_IF_UNLIKELY_NULL(lpszName, NULL_OBJECT);
	EXIT_IF_CONDITION(16384 < ulHeight, NULL_OBJECT);
	EXIT_IF_CONDITION(16384 < ulWidth, NULL_OBJECT);

	HANDLE hWindow;	
	LPRWINDOW lprWindow;
	lprWindow = GlobalAllocAndZero(sizeof(RWINDOW));
	EXIT_IF_UNLIKELY_NULL(lprWindow, NULL_OBJECT);


	StringCopySafe(
		lprWindow->csWindowName,
		lpszName,
		sizeof(lprWindow->csWindowName) - 1
	);

	lprWindow->ulFlags = ulFlags;
	lprWindow->ulPositionX = ulPositionX;
	lprWindow->ulPositionY = ulPositionY;
	lprWindow->ulHeight = ulHeight;
	lprWindow->ulWidth = ulWidth;

	/* For OpenGL */
	if (0 != (ulFlags & SDL_WINDOW_OPENGL))
	{
		/*SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);*/
		SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE, 32);
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	}

	lprWindow->lpsdlWindow = SDL_CreateWindow(
		lprWindow->csWindowName,
		lprWindow->ulPositionX,
		lprWindow->ulPositionY,
		lprWindow->ulWidth,
		lprWindow->ulHeight,
		lprWindow->ulFlags
	);

	

	/* More OpenGL */
	if (0 != (ulFlags & SDL_WINDOW_OPENGL))
	{ lprWindow->lpglContext = SDL_GL_CreateContext(lprWindow->lpsdlWindow); }
	
	glewInit();

	hWindow = CreateHandleWithSingleInheritor(
		lprWindow,
		&(lprWindow->hThis),
		HANDLE_TYPE_RENDERABLE_WINDOW,
		__DestroyRenderableWindow,
		0,
		NULLPTR,
		0,
		NULLPTR,
		0,
		&(lprWindow->ullId),
		0
	);

	return hWindow;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
RenderableWindowShow(
	_In_ 		HANDLE 			hWindow
){
	EXIT_IF_UNLIKELY_NULL(hWindow, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindow), HANDLE_TYPE_RENDERABLE_WINDOW, FALSE);

	LPRWINDOW lprWindow;
	lprWindow = (LPRWINDOW)GetHandleDerivative(hWindow);
	EXIT_IF_UNLIKELY_NULL(lprWindow, FALSE);

	SDL_ShowWindow(lprWindow->lpsdlWindow);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
RenderableWindowHide(
	_In_		HANDLE 			hWindow
){
	EXIT_IF_UNLIKELY_NULL(hWindow, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindow), HANDLE_TYPE_RENDERABLE_WINDOW, FALSE);

	LPRWINDOW lprWindow;
	lprWindow = (LPRWINDOW)GetHandleDerivative(hWindow);
	EXIT_IF_UNLIKELY_NULL(lprWindow, FALSE);

	SDL_HideWindow(lprWindow->lpsdlWindow);

	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
RenderableWindowGetSize(
	_In_ 		HANDLE 			hWindow,
	_Out_ 		LPULONG			lpulWidth,
	_Out_ 		LPULONG			lpulHeight
){
	EXIT_IF_UNLIKELY_NULL(hWindow, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindow), HANDLE_TYPE_RENDERABLE_WINDOW, FALSE);

	LPRWINDOW lprWindow;
	LONG lX, lY;
	lprWindow = (LPRWINDOW)GetHandleDerivative(hWindow);
	EXIT_IF_UNLIKELY_NULL(lprWindow, FALSE);

	SDL_GetWindowSize(
		lprWindow->lpsdlWindow,
		&lX,
		&lY
	);

	if (NULLPTR != lpulWidth)
	{ *lpulWidth = (ULONG)lX; }

	if (NULLPTR != lpulHeight)
	{ *lpulHeight = (ULONG)lY; }


	return TRUE;
}




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
RenderableWindowSwapBuffer(
	_In_ 		HANDLE 			hWindow
){
	EXIT_IF_UNLIKELY_NULL(hWindow, FALSE);
	EXIT_IF_UNLIKELY_NOT_VALUE(GetHandleDerivativeType(hWindow), HANDLE_TYPE_RENDERABLE_WINDOW, FALSE);

	LPRWINDOW lprWindow;
	lprWindow = (LPRWINDOW)GetHandleDerivative(hWindow);
	EXIT_IF_UNLIKELY_NULL(lprWindow, FALSE);

	if (0 != (lprWindow->ulFlags & SDL_WINDOW_OPENGL))
	{ SDL_GL_SwapWindow(lprWindow->lpsdlWindow); }

	return TRUE;
}