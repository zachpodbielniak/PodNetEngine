#ifndef PODNETENGINE_WINDOWOBJECT_C
#define PODNETENGINE_WINDOWOBJECT_C


#include "CWindow.h"


typedef struct __RWINDOW
{
	INHERITS_FROM_HANDLE();
	HANDLE 		hLock;
	HANDLE 		hRenderer;
	LPSDLWINDOW	lpsdlWindow;
	LPSDLGLCONTEXT	lpglContext;

	ULONG 		ulPositionX;
	ULONG 		ulPositionY;
	ULONG 		ulWidth;
	ULONG 		ulHeight;

	CSTRING 	csWindowName[0x100];
	ULONG 		ulFlags;
} RWINDOW, *LPRWINDOW;


#endif