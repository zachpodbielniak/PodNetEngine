#ifndef PODNETENGINE_WINDOW_H
#define PODNETENGINE_WINDOW_H


#include "../Prereqs.h"
#include "../Defs.h"
#include "../TypeDefs.h"



_Success_(return != NULL_OBJECT, _Non_Locking_)
PODNETENGINE_API
HANDLE
CreateRenderableWindow(
	_In_Z_		LPSTR RESTRICT		lpszName,
	_In_ 		ULONG 			ulPositionX,
	_In_ 		ULONG 			ulPositionY,
	_In_ 		ULONG 			ulHeight,
	_In_ 		ULONG 			ulWidth,
	_In_Opt_ 	ULONG 			ulFlags
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
RenderableWindowShow(
	_In_ 		HANDLE 			hWindow
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
RenderableWindowHide(
	_In_		HANDLE 			hWindow
);



_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
RenderableWindowHasFlag(
	_In_ 		ULONG 			ulFlag
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
RenderableWindowGetSize(
	_In_ 		HANDLE 			hWindow,
	_Out_ 		LPULONG			lpulWidth,
	_Out_ 		LPULONG			lpulHeight
);




_Success_(return != FALSE, _Non_Locking_)
PODNETENGINE_API
BOOL
RenderableWindowSwapBuffer(
	_In_ 		HANDLE 			hWindow
);





#endif